//
//  HomeViewController.swift
//  Weedora
//
//  Created by Muhammad Abdullah on 01/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit
import LGSideMenuController

enum Storyboards {
    case Main
    var id: String {
        return String(describing: self)
    }
}

class homeCollectionViewCell : UICollectionViewCell {
    @IBOutlet weak var categoryTitle: UILabel!
    
}

class HomeViewController: BaseViewController {
    
    @IBOutlet weak var homeCollectionView: UICollectionView!
    @IBOutlet weak var categoryView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let backBtn = UIBarButtonItem(image: UIImage(named: "menu")?.withRenderingMode(.alwaysOriginal), style: .done, target: self, action: #selector(tappedSideMenu(_ :)))
            backBtn.tintColor = .white
            self.navigationItem.leftBarButtonItem = backBtn

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        self.categoryView.addGestureRecognizer(tapGesture)
        self.categoryView.layer.cornerRadius = 8.0
        
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
           print("tap")
        self.pushController(controller: ProductsViewController.id, storyboard: Storyboards.Main.id)
    }
    
    @objc func tappedSideMenu(_ sender: UIButton){
            showLeftViewAnimated(sender)
    }

}

extension HomeViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        print(homeCollectionView.frame.width)
        return CGSize(width: AppGlobals.FULL_SCREEN_WIDTH, height: 200)
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = homeCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! homeCollectionViewCell
        cell.categoryTitle.text = "Strains"
        cell.layer.cornerRadius = 5.0
        return cell
    }
}
