//
//  ProductDetailViewController.swift
//  Weedora
//
//  Created by Muhammad Abdullah on 01/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit
import DropDown
import AnimatedField


class ProductDetailViewController: BaseViewController {
    
    @IBOutlet weak var productImageVu: UIImageView!
    @IBOutlet weak var productDescriptionTxtVu: UITextView! {
        didSet{
            productDescriptionTxtVu.isHidden = false
        }
    }
    @IBOutlet weak var salePricelbl: UILabel!
    @IBOutlet weak var downArrow: UIButton!
    @IBOutlet weak var productPriceLbl: UILabel!
    @IBOutlet weak var addToOrderBtn: UIButton! {
        didSet{
            addToOrderBtn.layer.cornerRadius = 8.0
        }
    }
    
    @IBOutlet weak var dropDownPicker: UIButton!
    
    
    var product: Json4Swift_Base?
    var selectedIndex: Int = 0
    let unitPicker = DropDown()
    var halfOZ = false
    var isMatched = false
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.unitPicker
        ]
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        self.addBackBtn()
        
        self.loadDropDown(["Per Oz", "1/2 Oz"])
        self.dropDownPicker.isUserInteractionEnabled = true
       
        if product?.dimensions?.length != "" {
            // it's a price of a packet
            productPriceLbl.text = "Price: $\(self.product?.dimensions?.length! ?? "")"
            let price = product?.sale_price != "" ? product?.sale_price : product?.regular_price
            self.salePricelbl.text = "Sale Price: $\(price ?? "")"
            self.dropDownPicker.isHidden = true
            self.downArrow.isHidden = true
            if product?.sale_price == "" {self.salePricelbl.isHidden=true}
            
//        } else if product?.weight != "" {
//            self.productPriceLbl.text = "Price: $\(product?.weight! ?? "")"
//            if product?.sale_price == "" {self.salePricelbl.isHidden=true}
//        }
        
    } else if product?.weight == "" && product?.dimensions?.length == "" && product?.sale_price == "" {
            
            self.downArrow.isHidden = true
            self.salePricelbl.isHidden = true
            self.dropDownPicker.setTitle("Per Oz", for: .normal)
            self.dropDownPicker.isUserInteractionEnabled = false
            self.productPriceLbl.text = "Price: $\(product?.regular_price! ?? "")"

        } else if product?.weight == "" && product?.dimensions?.length == "" {
            
            self.downArrow.isHidden = true
            self.dropDownPicker.setTitle("Per Oz", for: .normal)
            self.dropDownPicker.isUserInteractionEnabled = false
            self.productPriceLbl.text = "Price: $\(product?.regular_price! ?? "")"
            self.salePricelbl.text = "Sale Price: $\(product?.sale_price! ?? "")"
            
        } else {
           
            self.dropDownPicker.isHidden = false
            self.downArrow.isHidden = false
            self.salePricelbl.isHidden = false
            self.productPriceLbl.isHidden = false
            if product?.sale_price == "" {self.salePricelbl.isHidden=true}
            let price = product?.sale_price != "" ? product?.sale_price : product?.regular_price
            self.salePricelbl.text = "Sale Price: $\(price ?? "")"
            productPriceLbl.text = "Price: $\(product?.regular_price! ?? "")"
        }
        
        self.title = product?.name!
        productDescriptionTxtVu.text = product?.short_description?.withoutHtmlTags
        
        
        if let productImageUrl = product?.images?[0].src, productImageUrl != ""  {
            print(productImageUrl)
            productImageVu?.af_setImage(withURL:URL.init(string: productImageUrl)!,placeholderImage:UIImage(named: "app_icon"))
        } else {
            productImageVu.image = UIImage(named: "app_icon")
        }
            productImageVu.contentMode = .scaleAspectFill
    }
    
    
    func checkOzOrHalf() -> (String,String) {
        
        if halfOZ == true || product?.weight != "" {
            return (product?.weight ?? "","0.5")
        }
        
        if product?.dimensions?.length != "" {return (product?.dimensions?.length ?? "","1.0")}
        // weight half oz and lenth is a packet price.
        if product?.weight == "" && product?.dimensions?.length == "" && product?.sale_price == "" {
            return (product?.regular_price ?? "","1.0")
        }
        if product?.weight == "" && product?.dimensions?.length == "" {
            return (product?.sale_price ?? "","1.0")
        } else {
            let price = product?.sale_price == "" ? product?.regular_price : product?.sale_price
            return (price ?? "","1.0")
        }
    }
    
    @IBAction func tappedAddToOrder(_ sender: Any) {
        let controllerref = self.getControllerRef(controller: CartListViewController.id, storyboard: Storyboards.Main.id) as! CartListViewController
        
        if AppGlobals.readCartData().count > 0 {
            
            for (index,item) in AppGlobals.readCartData().enumerated() {
                if self.product?.id! == item.id {
                    isMatched = true
                    self.updateProduct(index, item)
                    break
                } else {
                    isMatched = false
                }
            }
            
            if isMatched {
            } else {
                addProduct()
            }
            
            
        } else {
            var arr = [Json4Swift_Base]()
            let price = checkOzOrHalf()
            self.product?.price = price.0
            self.product?.stock_quantity = price.1
            arr.append(self.product!)
            AppGlobals.shared.saveData(arr)
        }
        
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(controllerref, animated: true)
        }
    }
    
    func addProduct(){
        var arr = AppGlobals.readCartData()
        let price = checkOzOrHalf()
        self.product?.price = price.0
        self.product?.stock_quantity = price.1
        arr.append(self.product!)
        AppGlobals.shared.saveData(arr)

    }
    
    func updateProduct(_ index: Int, _ item: Json4Swift_Base){
        
        var arr = AppGlobals.readCartData()
        
        if self.product?.id! == item.id {
           // arr.remove(at: index)
            var qty = Double(item.stock_quantity!)
                        
            //just update the quantity here
            let price = checkOzOrHalf()
            self.product?.price = "\(Double(price.0)!)"
            qty = qty! + Double(price.1)!
            self.product?.stock_quantity = "\(qty!)"
            arr[index] = self.product!
            print(arr)
           // arr.append(self.product!)
            AppGlobals.shared.saveData(arr)
        }

        
    }
    @IBAction func tappedPicker(_ sender: Any) {
        self.unitPicker.show()
    }
        
    func loadDropDown(_ types : [String]){
         
        unitPicker.dataSource = types
        unitPicker.anchorView = dropDownPicker
        unitPicker.direction = .any
        unitPicker.dismissMode = .onTap
        
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectedTextColor = UIColor.white
        DropDown.appearance().textFont = UIFont.systemFont(ofSize: 15)
        DropDown.appearance().selectionBackgroundColor = #colorLiteral(red: 0, green: 0.1882352941, blue: 0.1490196078, alpha: 1)
        DropDown.appearance().cornerRadius = 2.5
        
        unitPicker.bottomOffset = CGPoint(x: 0, y: dropDownPicker.bounds.height)
        
        // by default show oz
        self.dropDownPicker.setTitle(types[0], for: .normal)
        
        // Action triggered on selection
        unitPicker.selectionAction = { [weak self] (index, item) in
            print(item)
            self?.dropDownPicker.setTitle(item, for: .normal)
            
            var price: String!
            if index == 0 { // per oz price
                self?.halfOZ = false
                if self?.product?.sale_price != "" && self?.product?.weight != "" {
                    price = self?.product?.regular_price
                } else if self?.product?.sale_price != "" {
                    price = self?.product?.sale_price
                } else {
                    price = self?.product?.regular_price
                }
            }
           
            if index == 1 { // half oz price
                price = self?.product?.weight!
                self?.halfOZ = true
            }
            
            self?.productPriceLbl.text = "Price: $\(price!)"
        }

     }

    
}

