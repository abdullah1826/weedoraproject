//
//  ProductsViewController.swift
//  Weedora
//
//  Created by Muhammad Abdullah on 01/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit
import AlamofireImage


class ProductsViewController: BaseViewController {

    @IBOutlet weak var productCollectionView: UICollectionView!
    @IBOutlet weak var searchTextFld: UITextField! {didSet{
        searchTextFld.delegate = self
        }}
    
    var selectedIndex: Int = 0
    private let spacing:CGFloat = 5.0
    
    var limit = 10
    var totalEntries = 100
    var fetchingMore = false
    var pageNumber = 1
    
    var isSearching = false
    
    var productsArray = [Json4Swift_Base]()
    var filterProductsArray = [Json4Swift_Base]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "Products"
        self.addBackBtn()
        
        self.productCollectionView.register(UINib(nibName: "ProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
            
            setCollectionLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hitFetchingMoreApi()
    }
    
    func hitFetchingMoreApi(){
        
        self.activityIndicator()
        // add 10 for the per page items
        if pageNumber > 1 {
            self.stopIndicator()
        } else {
            
        }
        NetworkCalls.getHomePageProducts(pageNumber, { (message, productsArr) in
            print(productsArr)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
               
                if self.productsArray.count == 0 {
                    self.productsArray = productsArr
                } else {
                    self.productsArray.append(contentsOf: productsArr)
                }
                self.productCollectionView.reloadData()
                self.fetchingMore = false
                self.stopIndicator()
            }
            
        }) { (error) in
            print(error)
            self.stopIndicator()
        }
    }
    
    func setCollectionLayout() {
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top:spacing,left:spacing,bottom:spacing,right:spacing)
        layout.minimumInteritemSpacing = spacing
        layout.minimumLineSpacing = spacing
        productCollectionView.collectionViewLayout = layout
    }
    
    func beginFetchingMore(){
        fetchingMore = true
        print("fetchingMore")
        
        pageNumber = pageNumber + 1
        print("pageNumber: \(pageNumber)")
        
        hitFetchingMoreApi()
    }
    
    func filteredArray(searchString:NSString){// we will use NSPredicate to find the string in array
          //  let predicate = NSPredicate(format: "SELF contains[c] %@",searchString) // This will give all element of array which contains search string
            //let predicate = NSPredicate(format: "SELF BEGINSWITH %@",searchString)// This will give all element of array which begins with search string
        
        filterProductsArray = productsArray.filter { (product: Json4Swift_Base) -> Bool in
            return product.name!.lowercased().contains(searchString.lowercased)
        }
            
        isSearching = true
        
        if filterProductsArray.count == 0 {
            filterProductsArray = self.productsArray
        }
     
        productCollectionView.reloadData()
        print(filterProductsArray)
    }

}

extension ProductsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let rows = self.isSearching == false ? productsArray.count : filterProductsArray.count
        return rows
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        
        let controllerRef = self.getControllerRef(controller: ProductDetailViewController.id, storyboard: Storyboards.Main.id) as! ProductDetailViewController

        if isSearching == false {
            let product = self.productsArray[indexPath.row]
            controllerRef.product = product
        } else {
            let product = self.filterProductsArray[indexPath.row]
            controllerRef.product = product
        }
        
        controllerRef.selectedIndex = selectedIndex
        self.navigationController?.pushViewController(controllerRef, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return  productsForIndexPath(indexPath: indexPath)
    }
    
    func productsForIndexPath(indexPath: IndexPath) -> UICollectionViewCell {

        let cell = productCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ProductCollectionViewCell
        
        cell.layer.cornerRadius = 8.0
        cell.layer.borderColor = #colorLiteral(red: 0, green: 0.1882352941, blue: 0.1490196078, alpha: 1)
        cell.layer.borderWidth = 1.0
        
        var product: Json4Swift_Base!
        if isSearching == true {
            product = filterProductsArray[indexPath.row]
        } else {
            product = productsArray[indexPath.row]

        }
        
        cell.productName.text = product.name!
        cell.stockStatus.text = product.stock_status!
        
        if product.stock_status! == "instock" {
            cell.stockStatus.textColor = #colorLiteral(red: 0.1294117719, green: 0.2156862766, blue: 0.06666667014, alpha: 1)
            cell.stockStatus.text = "In Stock"
            
        } else {
            cell.stockStatus.textColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            cell.stockStatus.text = "Out Stock"
        }
        
        if product.images?.count != 0 {
            if let productImageUrl = product.images?[0].src, productImageUrl != ""  {
                print(productImageUrl)
                cell.productImageVu?.af_setImage(withURL:URL.init(string: productImageUrl)!,placeholderImage:UIImage(named: "app_icon"))
            }
        }  else {
                cell.productImageVu.image = UIImage(named: "app_icon")
        }

        
        
        cell.productImageVu.contentMode = .scaleAspectFill
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numberOfItemsPerRow:CGFloat = 2
        let spacingBetweenCells:CGFloat = 5
        
        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        print(totalSpacing)
        
        if let collection = self.productCollectionView {
                 let width = (collection.bounds.width - totalSpacing)/numberOfItemsPerRow
                 return CGSize(width: width, height: 250)
             }else{
                 return CGSize(width: 0, height: 0)
             }

    }
}
extension ProductsViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
      //  let pageNumber = round(scrollView.contentOffset.x / self.productCollectionView.frame.size.width)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        //get off set
        let offSetY = scrollView.contentOffset.y
        //get total height
        let contentHeight = scrollView.contentSize.height
        
        if offSetY > contentHeight - scrollView.frame.height {
            
            if !fetchingMore {
                beginFetchingMore()
            }
        }
     
    }

}

extension ProductsViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        let string1 = string
        let string2 = textField.text
        var finalString = ""
        
        if string.count > 0 { // if it was not delete character
            finalString = string2! + string1
        }
        else if string2!.count > 0 { // if it was a delete character
            
            finalString = String(string2!.dropLast())
        }
        // pass the search String in this method
        self.filteredArray(searchString: finalString as NSString)
        return true
        
    }
}
