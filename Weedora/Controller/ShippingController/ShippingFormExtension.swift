//
//  ShippingFormExtension.swift
//  Weedora
//
//  Created by Muhammad Abdullah on 01/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import Foundation
import DropDown
import AnimatedField

extension ShippingFormViewController {
    

    func setCornerRadius(){
        
        backBtn.layer.cornerRadius = 5.0
        backBtn.layer.borderColor = UIColor.white.cgColor
        backBtn.layer.borderWidth = 1.0
        
        doneBtn.layer.cornerRadius = 5.0
        doneBtn.layer.borderColor = #colorLiteral(red: 0, green: 0.1882352941, blue: 0.1490196078, alpha: 1)
        doneBtn.layer.borderWidth = 1.0
        
    }

    func setAnimatedTF(){
        
        var format = AnimatedFieldFormat()
        
        format.titleFont = UIFont(name: "Nexa Light", size: 14)!
        format.textFont = UIFont(name: "Nexa Light", size: 14)!
        format.alertColor = .red
        format.alertFieldActive = false
        format.titleAlwaysVisible = false
        format.alertEnabled = true
        format.alertFont = UIFont(name: "Nexa Light", size: 14)!
        
        /// Font for counter
        format.counterFont = UIFont.systemFont(ofSize: 13, weight: .regular)
        /// Line color
        format.lineColor = #colorLiteral(red: 0, green: 0.1882352941, blue: 0.1490196078, alpha: 1)
        /// Title label text color
        format.titleColor = UIColor.lightGray
        /// TextField text color
        format.textColor = UIColor.black
        /// Counter text color
           
        fullNameTF.placeholder = "Full Name"
        fullNameTF.format = format
        fullNameTF.type = .none
        fullNameTF.dataSource = self
        fullNameTF.delegate = self
        fullNameTF.tag = 1
        
        phoneNumberTF.placeholder = "Phone Number"
        phoneNumberTF.format = format
        phoneNumberTF.type = .none
        phoneNumberTF.dataSource = self
        phoneNumberTF.delegate = self
        phoneNumberTF.lowercased = true
        phoneNumberTF.tag = 2
        
        emailTF.placeholder = "Email"
        emailTF.format = format
        emailTF.type = .email
        emailTF.dataSource = self
        emailTF.delegate = self
        emailTF.lowercased = true
        emailTF.tag = 3
        
        suiteTF.placeholder = "Suite"
        suiteTF.format = format
        suiteTF.type = .none
        suiteTF.keyboardType = .numberPad
        suiteTF.dataSource = self
        suiteTF.delegate = self
        suiteTF.lowercased = false
        suiteTF.tag = 4
        
        postalCodeTF.placeholder = "Postal Code"
        postalCodeTF.format = format
        postalCodeTF.type = .none
        postalCodeTF.dataSource = self
        postalCodeTF.delegate = self
        postalCodeTF.lowercased = false
        postalCodeTF.tag = 5

        majorIntersections.placeholder = "Major Intersections"
        majorIntersections.text = "Major Intersections"
        majorIntersections.format = format
        majorIntersections.type = .multiline
        majorIntersections.dataSource = self
        majorIntersections.delegate = self
        majorIntersections.tag = 6

        deliveryDetailsTF.placeholder = "Delivery Details"
        deliveryDetailsTF.format = format
        deliveryDetailsTF.type = .none
        deliveryDetailsTF.dataSource = self
        deliveryDetailsTF.delegate = self
        deliveryDetailsTF.tag = 7
        
    
    }
    
    
    func loadStates(_ states : [String]){
        
        stateDropDown.anchorView = stateTF
        stateDropDown.direction = .any
        stateDropDown.dismissMode = .onTap
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectedTextColor = UIColor.white
        DropDown.appearance().textFont = UIFont(name: "Nexa Light", size: 10)!
        DropDown.appearance().selectionBackgroundColor = #colorLiteral(red: 0, green: 0.1882352941, blue: 0.1490196078, alpha: 1)
        DropDown.appearance().cornerRadius = 2.5
        
        
        stateDropDown.bottomOffset = CGPoint(x: 0, y: stateTF.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        stateDropDown.dataSource = states
        
        // Action triggered on selection
        stateDropDown.selectionAction = { [weak self] (index, item) in
                self?.stateTF.text = item
                self?.stateTF.placeholder = ""
        }
    }
    
    func loadDeliveryTypes(_ types : [String]){
        
        deliveryTypeDropDown.anchorView = deliveryDetailsTF
        deliveryTypeDropDown.direction = .any
        deliveryTypeDropDown.dismissMode = .onTap
        
        let appearance = DropDown.appearance()
        
        appearance.backgroundColor = UIColor.white
        appearance.selectedTextColor = UIColor.white
        appearance.textFont = UIFont.systemFont(ofSize: 15)
        appearance.textColor = UIColor.black
        appearance.selectionBackgroundColor = #colorLiteral(red: 0, green: 0.1882352941, blue: 0.1490196078, alpha: 1)
        appearance.cornerRadius = 2.5
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 25
        appearance.animationduration = 0.25
        
        if #available(iOS 11.0, *) {
            appearance.setupMaskedCorners([.layerMaxXMaxYCorner, .layerMinXMaxYCorner])
        }
        
        deliveryTypeDropDown.bottomOffset = CGPoint(x: 0, y: deliveryDetailsTF.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        deliveryTypeDropDown.dataSource = types
        
        // Action triggered on selection
        deliveryTypeDropDown.selectionAction = { [weak self] (index, item) in
            self?.selectDeliveryTimeTF.text = item
            self?.selectDeliveryTimeTF.placeholder = ""
        }
    }
    
    
 /*   func updateCustomer(_ customer: Customer){
        
        if let houseNumber = customer.shipping?.address_1 {
            self.houseNumber.text = houseNumber.replace(string: "%20", replacement: " ")
        }
        
        if let streetNumber = customer.shipping?.address_2 {
            self.streetNumber.text = streetNumber.replace(string: "%20", replacement: " ")
        }
        
        if let username = customer.username {
            customerName.text = username.replace(string: "%20", replacement: " ")
        }
        
        if let customerPhone = customer.billing?.phone {
            self.customerPhone.text = customerPhone.replace(string: "%20", replacement: " ")
        }
        
        if let location = customer.shipping?.city {
            locationTF.text = location.replace(string: "%20", replacement: " ")
        }
        
        if let state = customer.shipping?.state {
            stateTF.text = state.replace(string: "%20", replacement: " ")
        }
        
        if let email = customer.email {
            emailAddress.text = email.replace(string: "%20", replacement: " ")
        }
        
        if let avatar_url = customer.avatar_url {
            print(avatar_url)
        }
    }
    

    
    
    func readJsonFileOfCities(){
        
        if let path = Bundle.main.path(forResource: "citieslist", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject>, let cities = jsonResult["cities"] as? [String] {
                    // do stuff
                    self.loadPaymentMethod(cities)
                }
            } catch {
                // handle error
            }
        }
        
    }
    
    
     func getCustomerData(){
        
        if let data = UserDefaults.standard.value(forKey:"customer") as? Data {
            let customer = try? PropertyListDecoder().decode(Customer.self, from: data)
            
            print(customer ?? 0)
            self.updateCustomer(customer!)
            
        }
    } */
}

