//
//  OrderDetailViewController.swift
//  Weedora
//
//  Created by Muhammad Abdullah on 02/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit

let SCREEN_WIDTH:CGFloat = CGFloat(UIScreen.main.bounds.width)
let SCREEN_HEIGHT:CGFloat = CGFloat(UIScreen.main.bounds.height)


class OrdersDetailTblVuCell: UITableViewCell {
    
    @IBOutlet weak var orderNamelbl: UILabel!
    @IBOutlet weak var orderPrice: UILabel!
    @IBOutlet weak var orderQty: UILabel!

}

class OrderDetailViewController: BaseViewController {

    @IBOutlet weak var topLbl: UILabel!
    @IBOutlet weak var tblVu: UITableView!
    
    @IBOutlet weak var backBtn: UIButton!
    
    var orderItems = [Order_items]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.topLbl.text = "Order Detail"
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }

    @IBAction func tappedBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension OrderDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderItems.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tblVu.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblVu.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OrdersDetailTblVuCell
        
        let currentOrder = orderItems[indexPath.row]
        cell.orderPrice.text = "$. " + "\(currentOrder.price ?? "")"
        cell.orderNamelbl.text = currentOrder.name!
        cell.orderQty.text =  "Quantity: \(currentOrder.quantity!)"
        
        return cell
    }
        
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

}

