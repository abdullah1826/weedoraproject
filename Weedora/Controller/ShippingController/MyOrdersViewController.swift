//
//  MyOrdersViewController.swift
//  Weedora
//
//  Created by Muhammad Abdullah on 02/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit
import Alamofire

class MyOrdersTblVuCell: UITableViewCell {
    
    @IBOutlet weak var orderNamelbl: UILabel!
    @IBOutlet weak var orderPrice: UILabel!
    @IBOutlet weak var orderIntersections: UILabel!
    @IBOutlet weak var orderDetails: UILabel!
    @IBOutlet weak var reorderBtn: UIButton!{didSet{
            reorderBtn.layer.cornerRadius = 8.0
        }}
}

class MyOrdersViewController: BaseViewController {
    
    @IBOutlet weak var tblVu: UITableView!
    
    var ordersArray = [PastOrders]()
    var selectedIndex: Int?
    var isfromSideMenu: Bool = false
    @IBOutlet weak var noOrdersVu: UIView! {didSet{
        noOrdersVu.isHidden = true
        }}
    
    @IBOutlet weak var clearOrdersBtn: UIButton!{didSet{
        clearOrdersBtn.layer.cornerRadius = 8.0
        }}
    
    @IBOutlet weak var keepShoppingBtn: UIButton!{didSet{
        keepShoppingBtn.layer.cornerRadius = 8.0
        }}
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblVu.delegate = self
        tblVu.dataSource = self
        tblVu.tableFooterView = UIView()

    }
    
    func getMyOrders() {
        
        if AppGlobals.readPastOrders().count > 0 {
            self.showloader()
            ordersArray = AppGlobals.readPastOrders()
            tblVu.reloadData()
            self.hideloader()
        } else {
             noOrdersVu.isHidden = false
             tblVu.isHidden = true
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        getMyOrders()
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
        
    @IBAction func tappedBackBtn(_ sender: Any) {
        sideMenuController?.showLeftViewAnimated(sender)
    }
    
    @IBAction func tappedKeepShoppingBtn(_ sender: Any) {
        
        let navigationController = sideMenuController?.rootViewController as! NavigationController
        let ref = self.getControllerRef(controller: ProductsViewController.id, storyboard: Storyboards.Main.id) as! ProductsViewController
        navigationController.pushViewController(ref, animated: true)

    }
    
    @IBAction func clearOrders(_ sender: Any) {
        if  AppGlobals.readPastOrders().count != 0 {
            self.showAlertWith(title: Constants.Error, message: "Do you want to delete all orders history?", onSuccess: {
                var orders = AppGlobals.readPastOrders()
                orders.removeAll()
                AppGlobals.saveOrder(orders)
                self.tblVu.reloadData()
                self.noOrdersVu.isHidden = false
            })
        }
    }
}

extension MyOrdersViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
            return "Past Orders"
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    private func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 50))
        headerView.backgroundColor = .lightGray
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ordersArray.count

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tblVu.deselectRow(at: indexPath, animated: true)
                
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
        let orderItems = self.ordersArray[indexPath.row].order_items
        vc.orderItems = orderItems!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblVu.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyOrdersTblVuCell
        
        let orders = ordersArray[indexPath.row]
        let orderItemsArray = orders.order_items
        var orderName = ""
        
        if orderItemsArray!.count > 1 {
            let price = orders.totalPrice
            cell.orderPrice.text = "$. " + "\(price ?? 0.0)"
            orderName = orders.order_items?[0].name ?? ""
            orderName = orderName + " & \(orderItemsArray!.count-1) more"
        } else {
            orderName = orders.order_items?[0].name ?? ""
            let price = orders.totalPrice
            cell.orderPrice.text = "$. " + "\(price ?? 0.0)"
        }
        
        cell.orderNamelbl.text = orderName
        cell.orderDetails.text = orders.detail?.withoutHtmlTags ?? ""
        cell.orderIntersections.text = orders.majorIntersections ?? ""

        cell.reorderBtn.addTarget(self, action: #selector(tappedReorder(_ :)), for: .touchUpInside)
        cell.reorderBtn.tag = indexPath.row

        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func tappedReorder (_ sender: UIButton){
        let navigationController = self.sideMenuController?.rootViewController as! NavigationController
        let ref = self.getControllerRef(controller: ShippingFormViewController.id, storyboard: Storyboards.Main.id) as! ShippingFormViewController
        ref.isFromReorder = true
        let orderItems = self.ordersArray[sender.tag].order_items
        ref.orderItemsArray = orderItems!
        navigationController.pushViewController(ref, animated: true)
    }
    
}
