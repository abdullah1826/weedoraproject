//
//  MyProfileViewController.swift
//  Weedora
//
//  Created by Muhammad Abdullah on 02/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit
import AnimatedField

class MyProfileViewController: BaseViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var fullNameTF: AnimatedField!
    @IBOutlet weak var phoneNumberTF: AnimatedField!
    @IBOutlet weak var emailTF: AnimatedField!
    @IBOutlet weak var locationTF: AnimatedField!
    @IBOutlet weak var updateBtn: UIButton!  {
           didSet{
               updateBtn.layer.cornerRadius = 8.0
           }
       }
    
    var isfromSideMenu: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.setAnimatedTF()
        
        guard let user = AppGlobals.readCustomerData(), user.email != "" else {return}
            fullNameTF.text = user.name ?? ""
            phoneNumberTF.text = user.phone ?? ""
            emailTF.text = user.email ?? ""
            locationTF.text = user.location ?? ""
            

    }
    @IBAction func tappedBackBtn(_ sender: Any) {
        sideMenuController?.showLeftViewAnimated(sender)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    @IBAction func tappedUpdateBtn(_ sender: Any) {
        
        if self.fullNameTF.text == "" {
            self.showAlertWith(title: Constants.Error, message: "Please Enter Name")
            return

        }

        if locationTF.text == "" {
            self.showAlertWith(title: Constants.Error, message: "Please Select Location")
            return
        }

        if emailTF.text == "" {
            self.showAlertWith(title: Constants.Error, message: "Please Enter Email Address")
            return

        }

        if phoneNumberTF.text == "" {
            self.showAlertWith(title: Constants.Error, message: "Please Enter Phone Number")
            return

        }
        
        guard var user = AppGlobals.readCustomerData(), user.email != "" else {
           let user = User(
            name: self.fullNameTF.text!,
            phone: self.phoneNumberTF.text!,
            email: self.emailTF.text!,
            location: self.locationTF.text!,
            suite: "",
            state: "",
            postalCode:"",
            deliveryTime: "",
            majorIntersections: "",
            deliveryDetails: "")
            AppGlobals.shared.saveCustomerData(user)
            self.showSuccess()
            return
        }
            user = User(
            name: self.fullNameTF.text!,
            phone: self.phoneNumberTF.text!,
            email: self.emailTF.text!,
            location: self.locationTF.text!,
            suite: user.suite!,
            state: user.state!,
            postalCode:user.postalCode!,
            deliveryTime: "",
            majorIntersections: "",
            deliveryDetails: "")
            AppGlobals.shared.saveCustomerData(user)
            self.showSuccess()
        }

    func setAnimatedTF(){
        
        var format = AnimatedFieldFormat()
        
        format.titleFont = UIFont(name: "Nexa Light", size: 14)!
        format.textFont = UIFont(name: "Nexa Light", size: 14)!
        format.alertColor = .red
        format.titleAlwaysVisible = false
        format.alertEnabled = true
        format.alertFont = UIFont(name: "Nexa Light", size: 14)!
        
        /// Font for counter
        format.counterFont = UIFont.systemFont(ofSize: 13, weight: .regular)
        /// Line color
        format.lineColor = #colorLiteral(red: 0, green: 0.1882352941, blue: 0.1490196078, alpha: 1)
        /// Title label text color
        format.titleColor = UIColor.lightGray
        /// TextField text color
        format.textColor = UIColor.black
        /// Counter text color
           
        fullNameTF.placeholder = "Full Name"
        fullNameTF.format = format
        fullNameTF.type = .none
        fullNameTF.dataSource = self
        fullNameTF.delegate = self
        fullNameTF.lowercased = false
        fullNameTF.tag = 1
        
        phoneNumberTF.placeholder = "Phone Number"
        phoneNumberTF.format = format
        phoneNumberTF.type = .none
        phoneNumberTF.dataSource = self
        phoneNumberTF.delegate = self
        phoneNumberTF.tag = 2
        
        emailTF.placeholder = "Email"
        emailTF.format = format
        emailTF.type = .email
        emailTF.dataSource = self
        emailTF.delegate = self
        emailTF.lowercased = true
        emailTF.tag = 3
        
        locationTF.placeholder = "Address"
        locationTF.format = format
        locationTF.type = .none
        locationTF.dataSource = self
        locationTF.delegate = self
        locationTF.tag = 4
        
    }
}

extension MyProfileViewController: AnimatedFieldDelegate, AnimatedFieldDataSource, UITextFieldDelegate {
    
    func animatedFieldDidEndEditing(_ animatedField: AnimatedField) {
        
        switch animatedField.tag {
        case 1:
            fullNameTF.text = animatedField.text
        case 2:
            phoneNumberTF.text = animatedField.text
        case 3:
            emailTF.text = animatedField.text
        case 4:
            locationTF.text = animatedField.text
        default:
            break
        }
    }
}
