//
//  ShippingFormViewController.swift
//  Weedora
//
//  Created by Muhammad Abdullah on 01/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit
import AnimatedField
import Alamofire
import DropDown
import GooglePlaces
import LGSideMenuController
//protocol moveDataBackToCartListDelegate {
//    func getSucess(_ status: Bool, _ customer: ResponseData)
//}

class ShippingFormViewController: BaseViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var fullNameTF: AnimatedField!
    @IBOutlet weak var phoneNumberTF: AnimatedField!
    @IBOutlet weak var emailTF: AnimatedField!
    @IBOutlet weak var locationTF: IBOutletTF! {
        didSet{
            locationTF.placeholder = "Location"
            locationTF.delegate = self
        }
    }
    @IBOutlet weak var suiteTF: AnimatedField!
    @IBOutlet weak var stateTF: DesignableTextField! {
        didSet{
            stateTF.delegate = self
            stateTF.placeholder = "Select City"
            stateTF.font = UIFont(name: "Nexa Light", size: 14)!
            stateTF.delegate = self
        }
    }
    @IBOutlet weak var txtVuTopLbl: UILabel!{didSet{
        txtVuTopLbl.isHidden = false
        }}
    @IBOutlet weak var postalCodeTF: AnimatedField!
    @IBOutlet weak var selectDeliveryTimeTF: DesignableTextField! {
        didSet{
            selectDeliveryTimeTF.delegate = self
            selectDeliveryTimeTF.placeholder = "Select Delivery Time"
            selectDeliveryTimeTF.font = UIFont(name: "Nexa Light", size: 14)!
            selectDeliveryTimeTF.delegate = self
        }
    }
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var majorIntersections: AnimatedField!
    @IBOutlet weak var deliveryDetailsTF: AnimatedField!
    @IBOutlet weak var specialinstructions: UITextView! {
        didSet{
            //            specialinstructions.layer.cornerRadius = 2.5
            //            specialinstructions.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            //            specialinstructions.layer.borderWidth = 1.5
            // specialinstructions.text = "Major Intersections"
        }
    }
    
    let deliveryTimeArray = ["12 to 5 PM","5 to 9 PM"]
    let stateArray = ["Toronto", "Mississauga", "North York", "Scarborough", "Brampton", "Etobicoke", "Durham Region"]
    
    //    var currentCustomer = [Customer]()
    //    var customerOrder = [Order]()
    
    let stateDropDown = DropDown()
    let deliveryTypeDropDown = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.stateDropDown,
            self.deliveryTypeDropDown
        ]
    }()
    
    //    var delegate: moveDataBackToCartListDelegate?
    
    var isFromReorder: Bool = false
    var orderItemsArray = [Order_items]()
    var totalPrice: Double = 0.0
    
    @IBOutlet weak var tappedStateArrow: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTap()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = true
        
        self.setAnimatedTF()
        self.setCornerRadius()
        
        self.loadStates(stateArray)
        self.loadDeliveryTypes(deliveryTimeArray)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        
        if AppGlobals.readCustomerData() != nil {
            let customer = AppGlobals.readCustomerData()!
            self.fullNameTF.text = customer.name
            self.phoneNumberTF.text = customer.phone
            self.emailTF.text = customer.email
            self.locationTF.text = customer.location
            self.suiteTF.text = customer.suite
            self.majorIntersections.text = ""//customer.majorIntersections
            self.deliveryDetailsTF.text = ""//customer.deliveryDetails
            self.postalCodeTF.text = customer.postalCode
            self.stateTF.text = customer.state
            self.selectDeliveryTimeTF.text = ""//customer.deliveryTime

        }
                
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func tappedDoneAccount(_ sender:UIButton){
        
                
        if self.fullNameTF.text == "" {
            self.showAlertWith(title: Constants.Error, message: "Please Enter Name")
            return

        }

        if locationTF.text == "" {
            self.showAlertWith(title: Constants.Error, message: "Please Select Location")
            return
        }

        if emailTF.text == "" {
            self.showAlertWith(title: Constants.Error, message: "Please Enter Email Address")
            return

        }

        if phoneNumberTF.text == "" {
            self.showAlertWith(title: Constants.Error, message: "Please Enter Phone Number")
            return

        }

        if stateTF.text == "" {
            self.showAlertWith(title: Constants.Error, message: "Please Select City")
            return
        }

        if postalCodeTF.text == "" {
            self.showAlertWith(title: Constants.Error, message: "Please Enter Postal Code")
            return
        }

        /* if majorIntersections.text == "" {
            self.showAlertWith(title: Constants.Error, message: "Please Enter Major Intersections")
            return
        } */
        
        if selectDeliveryTimeTF.text == "" {
            self.showAlertWith(title: Constants.Error, message: "Please Select Delivery Time")
            return
        }
        
        
         self.placingorder()

        
        
        
//                self.fullNameTF.text = "Muhammad Abdullah"
//                self.emailTF.text = "abdullah@gmail.com"
//                self.locationTF.text = "Lahore Pakistan"
//                self.suiteTF.text = "121"
//                self.majorIntersections.text = "major"
//                self.deliveryDetailsTF.text = "Delivery should be asap"
//                self.postalCodeTF.text = "54000"
//                self.stateTF.text = "toronto"
//                self.selectDeliveryTimeTF.text = "9 to 5"
        
        
//               let product = AppGlobals.readCartData()
//                // fetch past orders
//                var pastOrdersArray = AppGlobals.readPastOrders()
//                self.totalPrice = 0.0
//
//            if self.isFromReorder == false {
//                self.orderItemsArray.removeAll()
//
//                for i in 0..<product.count {
//                    let order = Order_items(
//                        name: product[i].name!,
//                        price: product[i].price!,
//                        quantity: product[i].stock_quantity!)
//                    // get total amount by multiply with qty and price
//                    self.totalPrice = self.totalPrice + product[i].price!.toDouble * product[i].stock_quantity!.toDouble
//                    self.orderItemsArray.append(order)
//                }
//
//                // add items to past orders
//                    let pastOrder = PastOrders(
//                        detail: self.deliveryDetailsTF.text,
//                        totalPrice: self.totalPrice,
//                        majorIntersections: self.majorIntersections.text!,
//                        order_items: self.orderItemsArray)
//                    print(pastOrder)
//                pastOrdersArray.append(pastOrder)
//
//        } else {
//
//                for i in 0..<self.orderItemsArray.count {
//                    let order = Order_items(
//                        name: self.orderItemsArray[i].name!,
//                        price: self.orderItemsArray[i].price!,
//                        quantity: self.orderItemsArray[i].quantity!)
//                    self.totalPrice = self.totalPrice + "\(self.orderItemsArray[i].price!)".toDouble
//                    self.orderItemsArray.append(order)
//                }
//                    let pastOrder = PastOrders(
//                        detail: self.deliveryDetailsTF.text,
//                        totalPrice: self.totalPrice,
//                        majorIntersections: self.majorIntersections.text!,
//                        order_items: self.orderItemsArray)
//
//                    print(pastOrder)
//                    pastOrdersArray.append(pastOrder)
//            }

        //
//        DispatchQueue.main.async {
//            // save user details from shipping form
//            //    self.saveCustomer(user)
//            // save order for past order
//            AppGlobals.saveOrder(pastOrdersArray)
//            // empty cart after placing the order
//            AppGlobals.shared.saveData([])
//            self.hideloader()
//            // move to HomeViewController after successfully placing the order
//            self.moveToHomeController()
//        }
    }
    
    @IBAction func tappedBackBtn(_ sender: Any) {
        self.backButtonAction()
    }
    
    
    func placingorder() {
        self.showloader()
        
        
        // read cart and make an object to send data through api for order placement
        let product = AppGlobals.readCartData()
        var mListProducts = [[String:Any]]()

        if isFromReorder == false {
            for i in 0..<product.count {
                let products: [String:Any] = [
                    "id":product[i].id!,
                    "image":product[i].images?[0].src ?? "",
                    "productName":product[i].name!,
                    "productPrice":product[i].price!,
                    "productQty":product[i].stock_quantity!,
                ]
                let price = product[i].price!
                totalPrice = totalPrice + "\(price)".toDouble
                mListProducts.append(products)
            }
        } else {
            
            for i in 0..<orderItemsArray.count {
                let products: [String:Any] = [
                    "id":"",
                    "image":"",
                    "productName":orderItemsArray[i].name!,
                    "productPrice":orderItemsArray[i].price!,
                    "productQty":orderItemsArray[i].quantity!
                ]
                let price = product[i].price!
                totalPrice = totalPrice + "\(price)".toDouble
                mListProducts.append(products)
            }
        }
                
      let parameters = [
        "address":self.locationTF.text!.addingPercentEncoding(withAllowedCharacters:.urlHostAllowed )!,
        "city":self.stateTF.text!,
        "comments":self.deliveryDetailsTF.text!.addingPercentEncoding(withAllowedCharacters:.urlHostAllowed )!,
        "deliveryTime":self.selectDeliveryTimeTF.text!,
        "email":self.emailTF.text!,
        "mListProducts":mListProducts,
        "majorIntersections":self.majorIntersections.text!.addingPercentEncoding(withAllowedCharacters:.urlHostAllowed )!,
        "name":self.fullNameTF.text!.addingPercentEncoding(withAllowedCharacters:.urlHostAllowed )!,
        "phone":self.phoneNumberTF.text!,
        "postalCode":self.postalCodeTF.text!,
        "suite": self.suiteTF.text != "" ? self.suiteTF.text! : "",
        "totalPrice": totalPrice
        ] as [String : Any]
                

            let jsonData = try? JSONSerialization.data(withJSONObject: parameters)
            
            let url = "http://email.weedora.ca/api/emailWeedora"
            let request = NSMutableURLRequest(url: NSURL(string: url)! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 20.0)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = nil
            request.httpBody = jsonData
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("\(jsonData!.count)", forHTTPHeaderField: "Content-Length")

            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if (error != nil) {
                    print(error!)
                } else {
                    
                    do {
                        
                        if let response = response {
                            print("url = \(response.url!)")
                            print("response = \(response)")
                            let httpResponse = response as! HTTPURLResponse
                            print("response code = \(httpResponse.statusCode)")
                        }
                        
                        guard let data = data, let _:URLResponse = response, error == nil else {
                            print("error")
                            return
                        }

                        let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
                        if let responseJSON = responseJSON as? [String: Any] {
                            print(responseJSON)
                            
                            self.showSuccess()
                            let user = self.updateUserObject()
                            
                            let product = AppGlobals.readCartData()
                            // fetch past orders
                            var pastOrdersArray = AppGlobals.readPastOrders()
                            self.totalPrice = 0.0
                            
                            if self.isFromReorder == false {
                                self.orderItemsArray.removeAll()
                                
                                for i in 0..<product.count {
                                    let order = Order_items(
                                        name: product[i].name!,
                                        price: product[i].price!,
                                        quantity: product[i].stock_quantity!)
                                    // get total amount by multiply with qty and price
                                    self.totalPrice = self.totalPrice + product[i].price!.toDouble * product[i].stock_quantity!.toDouble
                                    self.orderItemsArray.append(order)
                                }
                                
                                // add items to past orders
                                let pastOrder = PastOrders(
                                    detail: self.deliveryDetailsTF.text,
                                    totalPrice: self.totalPrice,
                                    majorIntersections: self.majorIntersections.text!,
                                    order_items: self.orderItemsArray)
                                print(pastOrder)
                                pastOrdersArray.append(pastOrder)
                                
                            } else {
                                
                                for i in 0..<self.orderItemsArray.count {
                                    let order = Order_items(
                                        name: self.orderItemsArray[i].name!,
                                        price: self.orderItemsArray[i].price!,
                                        quantity: self.orderItemsArray[i].quantity!)
                                    self.totalPrice = self.totalPrice + "\(self.orderItemsArray[i].price!)".toDouble
                                    self.orderItemsArray.append(order)
                                }
                                let pastOrder = PastOrders(
                                    detail: self.deliveryDetailsTF.text,
                                    totalPrice: self.totalPrice,
                                    majorIntersections: self.majorIntersections.text!,
                                    order_items: self.orderItemsArray)
                                
                                print(pastOrder)
                                pastOrdersArray.append(pastOrder)
                            }
                            
                            DispatchQueue.main.async {
                                // save user details from shipping form
                                self.saveCustomer(user)
                                // save order for past order
                                AppGlobals.saveOrder(pastOrdersArray)
                                // empty cart after placing the order
                                AppGlobals.shared.saveData([])
                                self.hideloader()
                                // move to HomeViewController after successfully placing the order
                                self.moveToHomeController()
                                
                            }

                        } else {
                            self.showFail()
                        }
                        
                        
                    } catch {
                        print("\nError")
                        self.showFail()
                    }
                }
            })
            
            dataTask.resume()
    }
    
    func updateUserObject () -> User{
        let user = User(
            name: self.fullNameTF.text!,
            phone: self.phoneNumberTF.text!,
            email: self.emailTF.text!,
            location: self.locationTF.text!,
            suite: self.suiteTF.text!,
            state: self.stateTF.text ?? "",
            postalCode: self.postalCodeTF.text!,
            deliveryTime: self.deliveryDetailsTF.text!,
            majorIntersections: self.majorIntersections.text!,
            deliveryDetails: self.deliveryDetailsTF.text!)
            print(user)
        return user
    }
    
    func moveToHomeController() {
        let navigationController = self.sideMenuController?.rootViewController as! NavigationController
        let ref = self.getControllerRef(controller: HomeViewController.id, storyboard: Storyboards.Main.id) as! HomeViewController
        navigationController.pushViewController(ref, animated: true)
    }
    
    func saveCustomer(_ customer: User) {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(customer), forKey: Constants.customer)
        UserDefaults.standard.synchronize()
    }
    
    @IBAction func tappedStateArrow(_ sender: Any) {
        stateDropDown.show()
        deliveryTypeDropDown.hide()
    }
    
    @IBAction func tappedTimeArrow(_ sender: Any) {
        deliveryTypeDropDown.show()
        stateDropDown.hide()
    }
    
        
}
extension ShippingFormViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.stateTF {
            self.view.endEditing(true)
            self.stateTF.resignFirstResponder()
            self.stateDropDown.show()
        }
        
        if textField == self.selectDeliveryTimeTF {
            self.view.endEditing(true)
            self.selectDeliveryTimeTF.resignFirstResponder()
            self.deliveryTypeDropDown.show()
        }
        
        if textField == self.locationTF {
            locationTF.text = ""
            locationTF.resignFirstResponder()
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            
            // Specify a filter.
            let filter = GMSAutocompleteFilter()
            // filter.type = .city
            filter.country = "CA"
            //  filter.type = .geocode
            autocompleteController.autocompleteFilter = filter
            present(autocompleteController, animated: true, completion: nil)
        }

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        }
}
//extension ShippingFormViewController: DesignableTextFieldDelegate {
//    func textFieldIconClicked(btn: UIButton) {
//        print("MyViewController : textFieldIconClicked")
//        if btn.tag == 0 {
//            stateDropDown.show()
//            deliveryTypeDropDown.hide()
//        } else {
//            deliveryTypeDropDown.show()
//            stateDropDown.hide()
//        }
//    }
//
//}
extension ShippingFormViewController: AnimatedFieldDelegate {
    
    func animatedFieldDidBeginEditing(_ animatedField: AnimatedField) {
        
        if animatedField == emailTF {
        } else if animatedField == stateTF {
            self.view.endEditing(true)
            stateDropDown.show()
            deliveryTypeDropDown.hide()
        } else if animatedField == selectDeliveryTimeTF {
            self.view.endEditing(true)
            deliveryTypeDropDown.show()
            stateDropDown.hide()
        }
        
        if animatedField == majorIntersections {
            self.majorIntersections.text = ""
            txtVuTopLbl.isHidden = true
        }
        
    }
        
    
    func animatedFieldDidEndEditing(_ animatedField: AnimatedField) {
        
        switch animatedField.tag {
        case 1: fullNameTF.text = animatedField.text
        case 2: phoneNumberTF.text = animatedField.text
        case 3: emailTF.text = animatedField.text
        case 4: suiteTF.text = animatedField.text
        case 5: postalCodeTF.text = animatedField.text
        case 6:
            
            if majorIntersections.text == "" {
                self.txtVuTopLbl.isHidden = false
            } else {
                self.txtVuTopLbl.isHidden = true
            }
            majorIntersections.text = animatedField.text
            
        case 7: deliveryDetailsTF.text = animatedField.text
        default:
            break
        }
        
    }
    
}
//
extension ShippingFormViewController: AnimatedFieldDataSource {
    
//    func animatedFieldLimit(_ animatedField: AnimatedField) -> Int? {
//        switch animatedField.tag {
//        case 1: return 30
//        case 8: return 300
//        default: return nil
//
//        }
//    }
    
    func animatedFieldValidationError(_ animatedField: AnimatedField) -> String? {
        if animatedField == emailTF {
            return "Email invalid! Please check again ;)"
        }
        return nil
    }
    
    
}
//

//MARK: GMSAutocompleteViewControllerDelegate
extension ShippingFormViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didSelect prediction: GMSAutocompletePrediction) -> Bool {
        return true
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
      UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }

    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
      UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // Get the place name from 'GMSAutocompleteViewController'
        // Then display the name in textField
        self.locationTF.text = place.name
                
        for addressComponent in (place.addressComponents)! {
            for type in (addressComponent.types){
                switch(type){
                    case "locality":
                        let locality = addressComponent.name
                        print(locality)
                      //  self.locationTF.text = locality
                default:
                    break
                }

            }
        }
        
        // Dismiss the GMSAutocompleteViewController when something is selected
        dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        dismiss(animated: true, completion: nil)
    }
}
