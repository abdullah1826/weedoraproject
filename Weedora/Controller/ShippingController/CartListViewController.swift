//
//  CartListViewController.swift
//  Weedora
//
//  Created by Muhammad Abdullah on 01/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit
import AlamofireImage

class CartItemsTblVuCell: UITableViewCell {
    @IBOutlet weak var productImgVu: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var variationName: UILabel!
    @IBOutlet weak var editProduct: UIButton!
}

class CartListViewController: BaseViewController {
    
    @IBOutlet weak var tblVu: UITableView! {
        didSet{
            tblVu.allowsSelection = false
        }
    }
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var buyNowBtn: UIButton! {
           didSet{
               buyNowBtn.layer.cornerRadius = 8.0
           }
       }
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var keepShoppingBtn: UIButton! {
        didSet{
            keepShoppingBtn.layer.cornerRadius = 8.0
        }
    }
    
    private var counterValue = 0
    var productCost : Int = 0
    var isfromBuyNow: Bool = false
    var isfromHome: Bool = false
    var grandAmount: Double = 0.0
    
    var simpleSelectedArray = [String]()
    var subMenuArr : [(Int,String,String)]?
    var selectedSubMenuArr : [(Int,String,String)]?
    
    var selectedItemIndex: Int = 0
    //       var data:ResponseData?
    //
    var cartItemsArray = [Json4Swift_Base]()
    //       var delegate: dissmissPerformed?
    
    var isfromSideMenu: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Cart Items"
        self.backGroundView.isHidden = false
        self.view.bringSubviewToFront(self.backGroundView)
        self.view.bringSubviewToFront(keepShoppingBtn)
        isfromSideMenu == true ? self.addMenuBtn() : self.addBackBtn()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0, green: 0.1882352941, blue: 0.1490196078, alpha: 1)
        self.getCartItems()
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    @IBAction func tappedKeepShoppingBtn(_ sender: Any) {
        
        let navigationController = sideMenuController?.rootViewController as! NavigationController
        let ref = self.getControllerRef(controller: ProductsViewController.id, storyboard: Storyboards.Main.id) as! ProductsViewController
        navigationController.pushViewController(ref, animated: true)

    }
    //MARK:- tappedBuyNowBtn
    @IBAction func tappedBuyNowBtn(_ sender: Any) {
        
        self.pushController(controller: ShippingFormViewController.id, storyboard: Storyboards.Main.id)
    }
    
    //MARK:- tappedCrossBtn
    @IBAction func tappedCrossBtn(_ sender: Any) {
        sideMenuController?.showLeftViewAnimated(sender)
    }
    
    func saveData(_ cartItemsArray: [Json4Swift_Base]){
        UserDefaults.standard.set(try? PropertyListEncoder().encode(cartItemsArray), forKey:Constants.items)
        UserDefaults.standard.synchronize()
    }
    
    func readData() -> [Json4Swift_Base]{
        var cartArray = [Json4Swift_Base]()
        if let data = UserDefaults.standard.value(forKey:Constants.items) as? Data {
            cartArray = try! PropertyListDecoder().decode(Array<Json4Swift_Base>.self, from: data)
            print(cartArray)
        }
        return cartArray
    }
    
    
    @objc func tappedMinusBtn(_ sender: UIButton){
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = tblVu.cellForRow(at: indexPath) as! CartItemsTblVuCell
        
        var arr = [Json4Swift_Base]()

        var qty = Double(cell.quantityLbl.text!)
        print(qty!)
        
        var amount: Double = 0.0
        let unit = checkOzOrHalf(indexPath.row) == "0.5" ? "0.5" : "1.0"
        print(unit)
        
        if(qty! != Double(unit)!){
            qty! -= Double(unit)!
            print(qty!)
            amount = Double(self.cartItemsArray[sender.tag].price?.replace(string: "$. ", replacement: "") ?? "0")!
            print(amount)
            grandAmount -= amount
            
            print(grandAmount)
            self.totalAmountLbl.text = "$. \(grandAmount)"
            
            print(qty!)
            cell.quantityLbl.text = "\(qty!)"
            
            var product = self.cartItemsArray[indexPath.row]
            product.stock_quantity = "\(qty!)"
//            product.price = "\(qty! * amount)"
            arr.append(product)
            AppGlobals.shared.saveData(arr)
            
        } else{
            
        }
        
        
        
    }
    
@objc func tappedPlusBtn(_ sender: UIButton){
    
    let indexPath = IndexPath(row: sender.tag, section: 0)
    let cell = tblVu.cellForRow(at: indexPath) as! CartItemsTblVuCell
    
    var arr = [Json4Swift_Base]()

    var qty = Double(cell.quantityLbl.text!)
    
    let unit = checkOzOrHalf(indexPath.row) == "0.5" ? "0.5" : "1.0"
    print(unit)
    
    if qty! != 8.0 {
        qty! += Double(unit)!
        print(qty!)
        cell.quantityLbl.text = "\(String(describing: qty!))"
        var amount = 0.0
        
        amount = self.calculateSingleProductPrice(sender.tag)
        grandAmount += amount
        print(grandAmount)
        self.totalAmountLbl.text = "$. \(grandAmount)"
        
        var product = self.cartItemsArray[indexPath.row]
//        product.price = "\(qty!*amount)"
        product.stock_quantity = "\(qty!)"
        arr.append(product)
        AppGlobals.shared.saveData(arr)

    } else {
        showAlertWith(title: Constants.Error, message: "maximum limit reached")
    }
    
    
    

}

func calculateSingleProductPrice (_ index: Int) -> Double {
    
    var totalAmount = 0.0
    
    if self.cartItemsArray.count > 0 {
        let cart = self.cartItemsArray[index]
        totalAmount = Double(cart.price!.replace(string: "$. ", replacement: ""))!
        print(totalAmount)
        
        self.buyNowBtn.alpha = 1.0
        self.buyNowBtn.isEnabled = true
        
    } else {
        self.totalAmountLbl.text = "$. 0"
        self.buyNowBtn.alpha = 0.5
        self.buyNowBtn.isEnabled = false
    }
    
    return totalAmount
}

    func checkOzOrHalf(_ indexPathRow: Int) -> String {
        
        if cartItemsArray[indexPathRow].weight != "" {
            return ("0.5")
        }
        
        if cartItemsArray[indexPathRow].dimensions?.length != "" {return "1.0"}
        // weight half oz and lenth is a packet price.
        if cartItemsArray[indexPathRow].weight == "" && cartItemsArray[indexPathRow].dimensions?.length == "" && cartItemsArray[indexPathRow].sale_price == "" {
            return "1.0"
        }
        if cartItemsArray[indexPathRow].weight == "" && cartItemsArray[indexPathRow].dimensions?.length == "" {
            return "1.0"
        } else {
            return "1.0"
        }
    }
    
func getCartItems(){
        
    self.showloader()
    if AppGlobals.readCartData().count > 0 {
        self.backGroundView.isHidden = true
        self.cartItemsArray = AppGlobals.readCartData()
        
        var totalAmount = 0.0
        for cart in cartItemsArray {
            print(cart.price!)
            if (cart.stock_quantity)?.toDouble == 0.5 {
                totalAmount += (cart.price!).toDouble * (2.0 *  (cart.stock_quantity)!.toDouble)
                print(totalAmount)
            } else {
                totalAmount += (cart.price!).toDouble * (cart.stock_quantity)!.toDouble
                print(totalAmount)
            }
        }
        
        self.grandAmount = totalAmount
        self.totalAmountLbl.text = "$. \(grandAmount)"
        self.tblVu.reloadData()
        self.hideloader()
        
    } else {
        self.backGroundView.isHidden = false
        self.view.bringSubviewToFront(self.backGroundView)
        self.view.bringSubviewToFront(keepShoppingBtn)
        self.grandAmount = 0
        UserDefaults.standard.removeObject(forKey: Constants.items)
        UserDefaults.standard.synchronize()
        self.hideloader()
    }
    
}

func culateTotalPrice(_ qty: Double) -> Double {
    
    var totalAmount = 0.0
    if self.cartItemsArray.count > 0 {
        for cart in cartItemsArray {
            totalAmount += (cart.price!).toDouble * qty
        }
        
        print(totalAmount)
        grandAmount = totalAmount
        //  self.totalAmountLbl.text = "USD. \(totalAmount)"
        self.buyNowBtn.alpha = 1.0
        self.buyNowBtn.isEnabled = true
        
    } else {
        
        self.totalAmountLbl.text = "$. 0"
        self.buyNowBtn.alpha = 0.5
        self.buyNowBtn.isEnabled = false
    }
    
    return totalAmount
}


func calculateRemainingTotalPrice(_ qty: Double, _ index:Int) -> Double {
    
    var totalAmount = 0.0
    
    if self.cartItemsArray.count > 0 {
        let cart = cartItemsArray[index]
        let itemPrice = (cart.price!).toDouble
        totalAmount =  itemPrice * qty
        print(totalAmount)
        self.buyNowBtn.alpha = 1.0
        self.buyNowBtn.isEnabled = true
    }
    
        return totalAmount
}

    func emptyCart(_ index: Int){
       // self.showloader()
        
    }

}


//extension CartListViewController: moveDataBackToCartListDelegate {
//
//    func getSucess(_ status: Bool, _ customer: ResponseData) {
//        print(customer)
//
//        // show alter to proceed the create order.
//        //  self.showOrderDialog(controller: self, customer)
//    }
//}
extension CartListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cartItemsArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblVu.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CartItemsTblVuCell
        
        let cartItem = self.cartItemsArray[indexPath.row]
        cell.productName.text = cartItem.name!
        _ = Double(cartItem.stock_quantity!)
        let price = Double(cartItem.price!)!
        cell.productPrice.text = "$: \(price)"
        cell.quantityLbl.text  = cartItem.stock_quantity!
        
        if let productImageUrl = cartItem.images?[0].src, productImageUrl != "" {
            print(productImageUrl)
            cell.productImgVu?.af_setImage(withURL:URL.init(string: productImageUrl)!,placeholderImage:UIImage(named: "app_icon"))
        } else {
            cell.productImgVu.image = UIImage(named: "app_icon")
        }
        
        cell.productImgVu.contentMode = .scaleAspectFill
        
        cell.minusBtn.addTarget(self, action: #selector(tappedMinusBtn(_ :)), for: .touchUpInside)
        cell.minusBtn.tag = indexPath.row
        cell.plusBtn.addTarget(self, action: #selector(tappedPlusBtn(_ :)), for: .touchUpInside)
        cell.plusBtn.tag = indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0  && 5 != 0 { return 10.0 } else { return 0 }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 { return 10.0 } else { return 0 }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            if AppGlobals.readCartData().count != 0 {
                let cart = AppGlobals.readCartData()[indexPath.row]
                var qty = Double(cart.stock_quantity!)
                print(qty!)
                print(grandAmount)
                let totalValue = Double(self.totalAmountLbl.text!.replace(string: "$. ", replacement: ""))!
                print(totalValue)
                
                if (cart.stock_quantity)?.toDouble == 0.5 {
                    qty =  (2.0 *  (cart.stock_quantity)!.toDouble)
                    print(qty!)
                }
                self.grandAmount = totalValue - self.calculateRemainingTotalPrice(qty!, indexPath.row)
                print(grandAmount)
                self.cartItemsArray.remove(at: indexPath.row)
                self.totalAmountLbl.text = "$. \(grandAmount)"
            }
            
            AppGlobals.shared.saveData(self.cartItemsArray)
            
            if AppGlobals.readCartData().count == 0 {
                self.totalAmountLbl.text = "$. \(0)"
                self.buyNowBtn.alpha = 0.5
                self.buyNowBtn.isEnabled = false
                self.backGroundView.isHidden = false
                self.view.bringSubviewToFront(self.backGroundView)
                self.view.bringSubviewToFront(keepShoppingBtn)
            } else {
                self.backGroundView.isHidden = true
            }
                self.tblVu.reloadData()
        }
    }
    
    @objc func editProductBtnTapped(_ sender: UIButton){
        
        //            let item = self.cartItemsArray![sender.tag]
        //
        //            let ref = self.getControllerRef(controller: MenuDetailViewController.id, storyboard: Storyboards.Main.id) as! MenuDetailViewController
        //            ref.isFromCartController = true
        //            ref.selectedItemIndex = Int(item.item_id!)!
        //            self.delegate?.dismissCart(true, item.item_id!)
        //            ref.modalPresentationStyle = .fullScreen
        //            self.present(ref, animated: true, completion: nil)
    }
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //
    //        let size =  CGSize(width: collectionView.frame.width, height:50)
    //        return size
    //    }
    
}
