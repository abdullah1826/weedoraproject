//
//  SignInViewController.swift
//  Weedora
//
//  Created by Muhammad Abdullah on 01/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit

class SignInViewController: BaseViewController {
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var eyeBtn: UIButton!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    var iconClick = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.emailField.text = "abdullah@gmail.com"
        self.passwordField.text = "123456"
    }
   
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
    
    @objc func tappedForgotPassword(_ sender: UIButton){
        
    }
    
    @IBAction func eyeAction(_ sender: Any) {
        
        if(iconClick == true) {
            passwordField.isSecureTextEntry = false
            eyeBtn.setImage(UIImage(named: "eye-visible") , for: .normal)
            
        } else {
            passwordField.isSecureTextEntry = true
            eyeBtn.setImage(UIImage(named: "eye-invisible") , for: .normal)
        }
        
        iconClick = !iconClick
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        
        if emailField.text == "" {
            return
        } else if passwordField.text == "" {
            return
        }
        
        if emailField.text != "" && passwordField.text != ""{
           // self.loginUser()
        }
    }
    
    @IBAction func signUpBtnTapped(_ sender: Any) {
        
    }
    
//    func loginUser() {
//
//      //  SVProgressHUD.show(withStatus: "Please wait, trying to login...")
//
//        guard let email = emailField.text, !email.isEmpty  else { return }
//        guard let password = passwordField.text, !password.isEmpty  else { return }
//
//        let params: [String: Any] = ["email": email, "password": password, "device_token": "Modelraksfhsfkhsqaqfhqqhoqeeqoifeqyq"]
//
//        self.showloader()
//
//        NetworkManager.shared().LOGIN(params: params, onSuccess: { (user
//            ) in
//            AppGlobals.removeUserData()
//            User.loggedUser = user
//            self.showSuccess()
//            if #available(iOS 13.0, *) {
//                sceneDelegate.userDidLoggedIn()
//            } else {
//                // Fallback on earlier versions
//                appDelegate.userDidLoggedIn()
//            }
//
//        }, onFailure: { (error) in
//            self.showFail()
//            let alert = UIAlertController(title: "Alert", message:error?.localizedDescription , preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//
//
//        }) { (mess) in
//            print("Login Failed")
//            self.showFail()
//
//            let alert = UIAlertController(title: "Alert", message: mess, preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//
//        }
//
//    }
}

