//
//  MenuController.swift
//  Weedora
//
//  Created by Muhammad Abdullah on 03/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit
import LGSideMenuController

class MainViewController: LGSideMenuController {
    
    private var type: UInt?
    
    override func viewDidLoad() {
    
        let regularStyle: UIBlurEffect.Style
        
        if #available(iOS 10.0, *) {
            regularStyle = .regular
        }
        else {
            regularStyle = .light
        }
        leftViewWidth = 300.0;
        leftViewPresentationStyle = .scaleFromBig
    }
    
    deinit {
        print("MainViewController deinitialized")
    }
    //Status bar changed
    override var prefersStatusBarHidden : Bool {
        return false
    }

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }

    override var preferredStatusBarUpdateAnimation : UIStatusBarAnimation {
        return .none
    }

    
}
