//
//  LeftViewController.swift
//  Weedora
//
//  Created by Muhammad Abdullah on 13/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit

enum LeftMenu {
    
    case Home
    case MyCart
    case RecentOrders
    case MyProfile
}

class lefMenuCell : UITableViewCell {
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
}

class LeftViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var fullNameLbl: UILabel!
    @IBOutlet weak var userImageVu: UIImageView! {didSet{
        userImageVu.layer.cornerRadius = userImageVu.frame.width / 2.0
        userImageVu.backgroundColor = .lightGray
        userImageVu.layer.borderColor = UIColor.black.cgColor
        userImageVu.layer.borderWidth = 1.0
        }}
    @IBOutlet weak var tblVu: UITableView!
    private let titlesArray = ["Home",
                               "My Cart",
                               "Recent Orders",
                               "My Profile",
                               ]
    private let iconsArray = ["home_tab.png",
                                "icons8-favorite_cart.png",
                                "ic_order.png",
                                "user.png",
    ]
        
    override func viewDidLoad() {
        super.viewDidLoad()
        tblVu.tableFooterView = UIView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        guard let user = AppGlobals.readCustomerData(), user.email != "" else {return}
        self.emailLbl.text = user.email!
        self.fullNameLbl.text = user.name!
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .fade
    }
    
    // MARK: - UITableViewDataSource
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titlesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! lefMenuCell

        cell.titleLbl.text = titlesArray[indexPath.row]
        let image = UIImage(named: iconsArray[indexPath.row])?.withRenderingMode(.alwaysTemplate)
        cell.icon.image = image
        cell.icon.tintColor = .black

        return cell
    }

    
    // MARK: - UITableViewDelegate
        
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let navigationController = sideMenuController?.rootViewController as! NavigationController
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        if indexPath.row == 0 {
            let ref = self.getControllerRef(controller: HomeViewController.id, storyboard: Storyboards.Main.id) as! HomeViewController
            navigationController.pushViewController(ref, animated: true)
            
        }
        if indexPath.row == 1 {
            let ref = self.getControllerRef(controller: CartListViewController.id, storyboard: Storyboards.Main.id) as! CartListViewController
            ref.isfromSideMenu = true
            navigationController.pushViewController(ref, animated: true)
        }
        
        if indexPath.row == 2 {
            let ref = self.getControllerRef(controller: MyOrdersViewController.id, storyboard: Storyboards.Main.id)
            navigationController.pushViewController(ref, animated: true)
        }
        if indexPath.row == 3 {
            
            let viewController = storyboard.instantiateViewController(identifier: "MyProfileViewController") as! MyProfileViewController
            viewController.isfromSideMenu = true
            navigationController.pushViewController(viewController, animated: true)
        }
        
        sideMenuController?.hideLeftView(animated: true, completionHandler: nil)

    }
    
}
