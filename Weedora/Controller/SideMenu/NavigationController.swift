//
//  NavigationController.swift
//  Weedora
//
//  Created by Muhammad Abdullah on 13/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {

    override var shouldAutorotate : Bool {
        return true
    }
        
    override var prefersStatusBarHidden : Bool {
        return false
    }

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }

    override var preferredStatusBarUpdateAnimation : UIStatusBarAnimation {
        return .none
    }

}
