//
//  NetworkCalls.swift
//  TacosTheBest
//
//  Created by Muhammad Abdullah on 03/04/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import UIKit
import NVActivityIndicatorView


class NetworkCalls {
    static let shared = NetworkCalls()
    private init(){}
    
    typealias ErrorType = (String) -> Void

    static func getHomePageProducts(_ pageNumber: Int, _ completion: @escaping(_ message: String, _ productsArr: [Json4Swift_Base]) -> Void, _ error: ErrorType){
                    
            if Utilites.isInternetAvailable() {
                
                let username = "ck_8cab57e2724966520fb44465e4d8b604b5ef78ca"
                let password = "cs_a009a0a9df12264856efaa029324411cb61e2ff6"
                                
                let url = URL(string: "http://weedora.ca/wp-json/wc/v3/products?per_page=10&page=\(pageNumber)&category=21&_fields=\(k_filterParams)&consumer_key=\(username)&consumer_secret=\(password)&orderby=title&order=asc")
                
                var request = URLRequest(url: url!)
                request.httpMethod = "GET"
                
                URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    print(error)
                    return
                }
                
                do {
                    if let response = response {
                        print("url = \(response.url!)")
                        print("response = \(response)")
                        let httpResponse = response as! HTTPURLResponse
                        print("response code = \(httpResponse.statusCode)")
                    }
                    
                    _ = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [[String:Any]]
                   // print(jsonResponseDict!)
                    
                    let jsonDecoder = JSONDecoder()
                    let responseModel = try! jsonDecoder.decode([Json4Swift_Base].self, from: data!)
                    
                    DispatchQueue.main.async {
                        completion(AlertConstants.Success, responseModel)
                    }
                    
                } catch let err {
                    
                    print("\nError", err.localizedDescription)
                    print("\nError", err)
                }
                
            }.resume()
        
        } else {error(AlertConstants.InternetNotReachable)}
    }
//    
//    static func getSubMenuItems(completion: @escaping (Dictionary<String,Any>) -> Void,
//                           error: @escaping ErrorType) {
//        if Utilites.isInternetAvailable() {
//            let url = APIConstants.BasePath + APIPaths.submenu
//            Alamofire.request(url,
//                              method: .get,
//                              encoding: JSONEncoding.default,
//                              headers: nil).responseJSON { (response) in
//                                switch response.result {
//                                case .success(let value):
//                                    let json = JSON(value)
//                                    print(json)
//                                        let data = json.dictionaryObject!
//                                        completion(data)
//                                case .failure(let err):
//                                    error(err.localizedDescription)
//                                }
//                            }
//        } else {
//            error(AlertConstants.InternetNotReachable)
//        }
//    }
//    
//    static func addToCart(_ params: [String:Any],completion: @escaping (Dictionary<String,Any>) -> Void,
//                          error: @escaping ErrorType){
//        
//        if Utilites.isInternetAvailable() {
//            
//            let jsonData = try? JSONSerialization.data(withJSONObject: params)
//            
//            let url = APIConstants.BasePath + APIPaths.addcart
//            let request = NSMutableURLRequest(url: NSURL(string: url)! as URL,
//                                              cachePolicy: .useProtocolCachePolicy,
//                                              timeoutInterval: 10.0)
//            request.httpMethod = "POST"
//            request.allHTTPHeaderFields = nil
//            request.httpBody = jsonData
//            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//            request.setValue("\(jsonData!.count)", forHTTPHeaderField: "Content-Length")
//
//            let session = URLSession.shared
//            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
//                if (error != nil) {
//                    print(error!)
//                } else {
//                    
//                    do {
//                        
//                        if let response = response {
//                            print("url = \(response.url!)")
//                            print("response = \(response)")
//                            let httpResponse = response as! HTTPURLResponse
//                            print("response code = \(httpResponse.statusCode)")
//                        }
//                        
//                        guard let data = data, let _:URLResponse = response, error == nil else {
//                            print("error")
//                            return
//                        }
//
//                        let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
//                        if let responseJSON = responseJSON as? [String: Any] {
//                            print(responseJSON)
//                            completion(responseJSON)
//                        }
//                        
//                    } catch let err {
//                        print("\nError", err)
//                    }
//                }
//            })
//            
//            dataTask.resume()
//            
//        } else {
//            error(AlertConstants.InternetNotReachable)
//        }
//    }
//    
//    static func updateCartItem(_ url: String, _ instruction: String,completion: @escaping (Dictionary<String,Any>) -> Void,
//                          error: @escaping ErrorType){
//        
//        if Utilites.isInternetAvailable() {
//            print(url)
////            let components = NSURLComponents(string: url)!
////            let queryItem = NSURLQueryItem(name: "item_instruction", value: instruction)
////            // add the query item to the URL, NSURLComponents takes care of adding the question mark.
////            components.queryItems = [queryItem]
////            // get the properly percent encoded string
////            url = components.string
//            let request = NSMutableURLRequest(url: NSURL(string: url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)! as URL,
//                                              cachePolicy: .useProtocolCachePolicy,
//                                              timeoutInterval: 10.0)
//            
//
//            request.httpMethod = "PUT"
//            request.allHTTPHeaderFields = nil
//            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//            
//            let session = URLSession.shared
//            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
//                if (error != nil) {
//                    print(error!)
//                } else {
//                    
//                    do {
//                        
//                        if let response = response {
//                            print("url = \(response.url!)")
//                            print("response = \(response)")
//                            let httpResponse = response as! HTTPURLResponse
//                            print("response code = \(httpResponse.statusCode)")
//                        }
//                        
//                        guard let data = data, let _:URLResponse = response, error == nil else {
//                            print("error")
//                            return
//                        }
//
//                        let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
//                        if let responseJSON = responseJSON as? [String: Any] {
//                            print(responseJSON)
//                            completion(responseJSON)
//                        }
//                        
//                    } catch let err {
//                        print("\nError", err)
//                    }
//                }
//            })
//            
//            dataTask.resume()
//            
//        } else {
//            error(AlertConstants.InternetNotReachable)
//        }
//    }
//    static func getSignleCartItem(_ params:[String:Any] ,completion: @escaping (CartSingleItem) -> Void,
//                           error: @escaping ErrorType) {
//        if Utilites.isInternetAvailable() {
//            let url = APIConstants.BasePath + APIPaths.getSignleCartItem
//            Alamofire.request(url,
//                              method: .post,
//                              parameters:params,
//                              encoding: JSONEncoding.default,
//                              headers: nil).responseJSON { (response) in
//                                switch response.result {
//                                case .success(let value):
//                                    let json = JSON(value)
//                                    print(json)
//
//                                    guard let data = response.data else { return }
//                                    
//                                    do {
//                                        let decoder = JSONDecoder()
//                                        let responseData = try! decoder.decode(CartSingleItem.self, from: data)
//                                        print(responseData)
//                                        completion(responseData)
//                                    } catch _ {
//                                }
//                                    
//                                case .failure(let err):
//                                    error(err.localizedDescription)
//                                }
//                            }
//        } else {
//            error(AlertConstants.InternetNotReachable)
//        }
//    }
//    static func getCartItems(_ params:[String:Any] ,completion: @escaping (CartItem) -> Void,
//                           error: @escaping ErrorType) {
//        if Utilites.isInternetAvailable() {
//            let url = APIConstants.BasePath + APIPaths.getcart
//            Alamofire.request(url,
//                              method: .post,
//                              parameters:params,
//                              encoding: JSONEncoding.default,
//                              headers: nil).responseJSON { (response) in
//                                switch response.result {
//                                case .success(let value):
//                                    let json = JSON(value)
//                                    print(json)
//
//                                    guard let data = response.data else { return }
//                                    
//                                    do {
//                                        let decoder = JSONDecoder()
//                                        let responseData = try! decoder.decode(CartItem.self, from: data)
//                                        print(responseData)
//                                        completion(responseData)
//                                    } catch _ {
//                                    }
//                                    
//                                case .failure(let err):
//                                    error(err.localizedDescription)
//                                }
//                            }
//        } else {
//            error(AlertConstants.InternetNotReachable)
//        }
//    }
//
//    static func deleteCartItems(_ params: [String:Any],completion: @escaping (Dictionary<String,Any>) -> Void,
//                          error: @escaping ErrorType){
//        
//        if Utilites.isInternetAvailable() {
//            
//          //  let jsonData = try? JSONSerialization.data(withJSONObject: params)
//            
//            let url = APIConstants.BasePath + APIPaths.deletecart + "/\(params["item_id"]!)" + "?user_id=\(params["user_id"]!)"
//            let request = NSMutableURLRequest(url: NSURL(string: url)! as URL,
//                                              cachePolicy: .useProtocolCachePolicy,
//                                              timeoutInterval: 10.0)
//            request.httpMethod = "DELETE"
//            request.allHTTPHeaderFields = nil
//            request.httpBody = nil
//            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//          //  request.setValue("\(jsonData!.count)", forHTTPHeaderField: "Content-Length")
//
//            let session = URLSession.shared
//            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
//                if (error != nil) {
//                    print(error!)
//                } else {
//                    
//                    do {
//                        
//                        if let response = response {
//                            print("url = \(response.url!)")
//                            print("response = \(response)")
//                            let httpResponse = response as! HTTPURLResponse
//                            print("response code = \(httpResponse.statusCode)")
//                        }
//                        
//                        guard let data = data, let _:URLResponse = response, error == nil else {
//                            print("error")
//                            return
//                        }
//
//                        let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
//                        if let responseJSON = responseJSON as? [String: Any] {
//                            print(responseJSON)
//                            completion(responseJSON)
//                        }
//                        
//                    } catch let err {
//                        print("\nError", err)
//                    }
//
//
//                }
//            })
//            
//            dataTask.resume()
//            
//        } else {
//            error(AlertConstants.InternetNotReachable)
//        }
//    }
//    

}
