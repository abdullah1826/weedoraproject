//
//  BaseViewController.swift
//
//  Created by Muhammad Abdullah on 27/03/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import LGSideMenuController

@available(iOS 13.0, *)

let k_backBtn = "back-white"
let sizeOfNVActivityIndicator = CGSize(width: 40, height: 40)


class BaseViewController: UIViewController, NVActivityIndicatorViewable  {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true

        overrideUserInterfaceStyle = .light


    }
    
    func activityIndicator(){
        self.startAnimating(sizeOfNVActivityIndicator, type: NVActivityIndicatorType.ballPulse, color: #colorLiteral(red: 0, green: 0.1882352941, blue: 0.1490196078, alpha: 1), fadeInAnimation: nil)
    }
    
    func stopIndicator(){
        self.stopAnimating()
    }
        
    func addBackBtn(){
                
        let backBtn = UIBarButtonItem(image: UIImage(named: k_backBtn)?.withRenderingMode(.alwaysOriginal), style: .done, target: self, action: #selector(tappedBackBtn))
            backBtn.tintColor = .white
            self.navigationItem.leftBarButtonItem = backBtn

    }
    
    @objc func tappedBackBtn(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func addMenuBtn(){
                  
       let backBtn = UIBarButtonItem(image: UIImage(named: "menu")?.withRenderingMode(.alwaysOriginal), style: .done, target: self, action: #selector(tappedMenuBtn(_ :)))
          backBtn.tintColor = .white
          self.navigationItem.leftBarButtonItem = backBtn

      }
      
    @objc func tappedMenuBtn(_ sender: UIButton){
        sideMenuController?.showLeftViewAnimated(sender)
      }
    
    func addDismissBtn(){
        
        let backBtn = UIBarButtonItem(image: UIImage(named: "arrow-point-to-right.png"), style: .done, target: self, action: #selector(tappedDismissBtn))
        backBtn.tintColor = .black
        self.navigationItem.leftBarButtonItem = backBtn

    }
    
    @objc func tappedDismissBtn(){
        self.dismiss(animated: true, completion: nil)
        
    }

    func pushController(controller toPush: String, storyboard: String) {
          let controller = UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier: toPush)
          self.navigationController?.pushViewController(controller, animated: true)
      }
      
      func getControllerRef(controller toPush: String, storyboard: String) -> UIViewController {
        return UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier: toPush)

      }
    
    func present(controller toPresent: String, storyboard: String) {
        let controller = UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(identifier: toPresent)
        self.present(controller, animated: true, completion: nil)
    }
    
    
    func showAlertWith(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString(AlertConstants.Ok.uppercased(), comment: ""), style: .`default`, handler: { _ in
        }))
        self.present(alert, animated: true, completion: nil)
    }

    func showAlertWith(title: String, message: String, onSuccess closure: @escaping () -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString(AlertConstants.Ok.uppercased(), comment: ""), style: .`default`, handler: { _ in
            closure()
        }))
        self.present(alert, animated: true, completion: nil)
    }

}
