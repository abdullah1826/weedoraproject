///*
//Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar
//
//*/
//
//import Foundation
////struct Links : Codable {
////    let selfArray : [SelfData]?
////    let collection : [Collection]?
////
////    enum CodingKeys: String, CodingKey {
////
////        case _self = "self"
////        case collection = "collection"
////    }
////
////    init(from decoder: Decoder) throws {
////        let values = try decoder.container(keyedBy: CodingKeys.self)
////        selfArray = try values.decodeIfPresent([SelfData].self, forKey: ._self)
////        collection = try values.decodeIfPresent([Collection].self, forKey: .collection)
////    }
////
////}
//
//struct TopLevel: Codable {
//    let id: Int
//    let name, slug, permalink, dateCreated: String
//    let dateCreatedGmt, dateModified, dateModifiedGmt, type: String
//    let status: String
//    let featured: Bool
//    let catalogVisibility, description, shortDescription, sku: String
//    let price, regularPrice, salePrice: String
//    let dateOnSaleFrom, dateOnSaleFromGmt, dateOnSaleTo, dateOnSaleToGmt: JSONNull?
//    let priceHTML: String
//    let onSale, purchasable: Bool
//    let totalSales: Int
//    let virtual, downloadable: Bool
//    let downloads: [JSONAny]
//    let downloadLimit, downloadExpiry: Int
//    let externalURL, buttonText, taxStatus, taxClass: String
//    let manageStock: Bool
//    let stockQuantity: Int
//    let stockStatus, backorders: String
//    let backordersAllowed, backordered, soldIndividually: Bool
//    let weight: String
//    let dimensions: Dimensions
//    let shippingRequired, shippingTaxable: Bool
//    let shippingClass: String
//    let shippingClassID: Int
//    let reviewsAllowed: Bool
//    let averageRating: String
//    let ratingCount: Int
//    let relatedIDS: [Int]
//    let upsellIDS, crossSellIDS: [JSONAny]
//    let parentID: Int
//    let purchaseNote: String
//    let categories: [Category]
//    let tags: [JSONAny]
//    let images: [TopLevelImage]
//    let attributes: [TopLevelAttribute]
//    let defaultAttributes: [JSONAny]
//    let variations: [Variation]
//    let groupedProducts: [JSONAny]
//    let menuOrder: Int
//    let metaData: [TopLevelMetaDatum]
//    let taxAmount, regularDisplayPrice, salesDisplayPrice, barcode: String
//    let links: Links
//
//    enum CodingKeys: String, CodingKey {
//        case id, name, slug, permalink
//        case dateCreated = "date_created"
//        case dateCreatedGmt = "date_created_gmt"
//        case dateModified = "date_modified"
//        case dateModifiedGmt = "date_modified_gmt"
//        case type, status, featured
//        case catalogVisibility = "catalog_visibility"
//        case description
//        case shortDescription = "short_description"
//        case sku, price
//        case regularPrice = "regular_price"
//        case salePrice = "sale_price"
//        case dateOnSaleFrom = "date_on_sale_from"
//        case dateOnSaleFromGmt = "date_on_sale_from_gmt"
//        case dateOnSaleTo = "date_on_sale_to"
//        case dateOnSaleToGmt = "date_on_sale_to_gmt"
//        case priceHTML = "price_html"
//        case onSale = "on_sale"
//        case purchasable
//        case totalSales = "total_sales"
//        case virtual, downloadable, downloads
//        case downloadLimit = "download_limit"
//        case downloadExpiry = "download_expiry"
//        case externalURL = "external_url"
//        case buttonText = "button_text"
//        case taxStatus = "tax_status"
//        case taxClass = "tax_class"
//        case manageStock = "manage_stock"
//        case stockQuantity = "stock_quantity"
//        case stockStatus = "stock_status"
//        case backorders
//        case backordersAllowed = "backorders_allowed"
//        case backordered
//        case soldIndividually = "sold_individually"
//        case weight, dimensions
//        case shippingRequired = "shipping_required"
//        case shippingTaxable = "shipping_taxable"
//        case shippingClass = "shipping_class"
//        case shippingClassID = "shipping_class_id"
//        case reviewsAllowed = "reviews_allowed"
//        case averageRating = "average_rating"
//        case ratingCount = "rating_count"
//        case relatedIDS = "related_ids"
//        case upsellIDS = "upsell_ids"
//        case crossSellIDS = "cross_sell_ids"
//        case parentID = "parent_id"
//        case purchaseNote = "purchase_note"
//        case categories, tags, images, attributes
//        case defaultAttributes = "default_attributes"
//        case variations
//        case groupedProducts = "grouped_products"
//        case menuOrder = "menu_order"
//        case metaData = "meta_data"
//        case taxAmount = "tax_amount"
//        case regularDisplayPrice = "regular_display_price"
//        case salesDisplayPrice = "sales_display_price"
//        case barcode
//        case links = "_links"
//    }
//}
//
//struct TopLevelAttribute: Codable {
//    let id: Int
//    let name: String
//    let position: Int
//    let visible, variation: Bool
//    let options: [String]
//}
//
//struct Category: Codable {
//    let id: Int
//    let name, slug: String
//}
//
//struct Dimensions: Codable {
//    let length, width, height: String
//}
//
//struct TopLevelImage: Codable {
//    let id: Int
//    let dateCreated, dateCreatedGmt, dateModified, dateModifiedGmt: String
//    let src, name, alt, the1536X1536: String
//    let the2048X2048, woocommerceThumbnail, woocommerceSingle, woocommerceGalleryThumbnail: String
//    let shopCatalog, shopSingle, shopThumbnail: String
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case dateCreated = "date_created"
//        case dateCreatedGmt = "date_created_gmt"
//        case dateModified = "date_modified"
//        case dateModifiedGmt = "date_modified_gmt"
//        case src, name, alt
//        case the1536X1536 = "1536x1536"
//        case the2048X2048 = "2048x2048"
//        case woocommerceThumbnail = "woocommerce_thumbnail"
//        case woocommerceSingle = "woocommerce_single"
//        case woocommerceGalleryThumbnail = "woocommerce_gallery_thumbnail"
//        case shopCatalog = "shop_catalog"
//        case shopSingle = "shop_single"
//        case shopThumbnail = "shop_thumbnail"
//    }
//}
//
//struct Links: Codable {
//    let purpleSelf, collection, up: [Collection]
//
//    enum CodingKeys: String, CodingKey {
//        case purpleSelf = "self"
//        case collection, up
//    }
//}
//
//struct Collection: Codable {
//    let href: String
//}
//
//struct TopLevelMetaDatum: Codable {
//    let id: Int
//    let key: String
//    let value: TentacledValue
//}
//
//enum TentacledValue: Codable {
//    case purpleValue(PurpleValue)
//    case string(String)
//
//    init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if let x = try? container.decode(String.self) {
//            self = .string(x)
//            return
//        }
//        if let x = try? container.decode(PurpleValue.self) {
//            self = .purpleValue(x)
//            return
//        }
//        throw DecodingError.typeMismatch(TentacledValue.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for TentacledValue"))
//    }
//
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        switch self {
//        case .purpleValue(let x):
//            try container.encode(x)
//        case .string(let x):
//            try container.encode(x)
//        }
//    }
//}
//
//struct PurpleValue: Codable {
//    let the32B0Bf150Bb6Bd30C74Ed5Fafdacd61F, the414C5E39686B80472Dfd19Eb68D5Cbda: The32B0Bf150Bb6Bd30C74Ed5Fafdacd61F
//
//    enum CodingKeys: String, CodingKey {
//        case the32B0Bf150Bb6Bd30C74Ed5Fafdacd61F = "32b0bf150bb6bd30c74ed5fafdacd61f"
//        case the414C5E39686B80472Dfd19Eb68D5Cbda = "414c5e39686b80472dfd19eb68d5cbda"
//    }
//}
//
//struct The32B0Bf150Bb6Bd30C74Ed5Fafdacd61F: Codable {
//    let expires: Int
//    let payload: [Payload]
//}
//
//struct Payload: Codable {
//    let id: Int
//}
//
//struct Variation: Codable {
//    let id: Int
//    let dateCreated, dateCreatedGmt, dateModified, dateModifiedGmt: String
//    let description, permalink, sku, price: String
//    let regularPrice, salePrice: String
//    let dateOnSaleFrom, dateOnSaleFromGmt, dateOnSaleTo, dateOnSaleToGmt: JSONNull?
//    let onSale: Bool
//    let status: String
//    let purchasable, virtual, downloadable: Bool
//    let downloads: [JSONAny]
//    let downloadLimit, downloadExpiry: Int
//    let taxStatus, taxClass: String
//    let manageStock: Bool
//    let stockQuantity: Int
//    let stockStatus, backorders: String
//    let backordersAllowed, backordered: Bool
//    let weight: String
//    let dimensions: Dimensions
//    let shippingClass: String
//    let shippingClassID: Int
//    let image: VariationImage
//    let attributes: [VariationAttribute]
//    let menuOrder: Int
//    let metaData: [VariationMetaDatum]
//    let variations: [JSONAny]
//    let taxAmount, regularDisplayPrice, salesDisplayPrice, barcode: String
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case dateCreated = "date_created"
//        case dateCreatedGmt = "date_created_gmt"
//        case dateModified = "date_modified"
//        case dateModifiedGmt = "date_modified_gmt"
//        case description, permalink, sku, price
//        case regularPrice = "regular_price"
//        case salePrice = "sale_price"
//        case dateOnSaleFrom = "date_on_sale_from"
//        case dateOnSaleFromGmt = "date_on_sale_from_gmt"
//        case dateOnSaleTo = "date_on_sale_to"
//        case dateOnSaleToGmt = "date_on_sale_to_gmt"
//        case onSale = "on_sale"
//        case status, purchasable, virtual, downloadable, downloads
//        case downloadLimit = "download_limit"
//        case downloadExpiry = "download_expiry"
//        case taxStatus = "tax_status"
//        case taxClass = "tax_class"
//        case manageStock = "manage_stock"
//        case stockQuantity = "stock_quantity"
//        case stockStatus = "stock_status"
//        case backorders
//        case backordersAllowed = "backorders_allowed"
//        case backordered, weight, dimensions
//        case shippingClass = "shipping_class"
//        case shippingClassID = "shipping_class_id"
//        case image, attributes
//        case menuOrder = "menu_order"
//        case metaData = "meta_data"
//        case variations
//        case taxAmount = "tax_amount"
//        case regularDisplayPrice = "regular_display_price"
//        case salesDisplayPrice = "sales_display_price"
//        case barcode
//    }
//}
//
//struct VariationAttribute: Codable {
//    let id: Int
//    let name, option: String
//}
//
//struct VariationImage: Codable {
//    let id: Int
//    let dateCreated, dateCreatedGmt, dateModified, dateModifiedGmt: String
//    let src, name, alt: String
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case dateCreated = "date_created"
//        case dateCreatedGmt = "date_created_gmt"
//        case dateModified = "date_modified"
//        case dateModifiedGmt = "date_modified_gmt"
//        case src, name, alt
//    }
//}
//
//struct VariationMetaDatum: Codable {
//    let id: Int
//    let key: String
//    let value: StickyValue
//}
//
//enum StickyValue: Codable {
//    case fluffyValue(FluffyValue)
//    case string(String)
//
//    init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if let x = try? container.decode(String.self) {
//            self = .string(x)
//            return
//        }
//        if let x = try? container.decode(FluffyValue.self) {
//            self = .fluffyValue(x)
//            return
//        }
//        throw DecodingError.typeMismatch(StickyValue.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for StickyValue"))
//    }
//
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        switch self {
//        case .fluffyValue(let x):
//            try container.encode(x)
//        case .string(let x):
//            try container.encode(x)
//        }
//    }
//}
//
//struct FluffyValue: Codable {
//    let the67696D537E07Ad54368A45E6E9A79Fd6: The32B0Bf150Bb6Bd30C74Ed5Fafdacd61F
//
//    enum CodingKeys: String, CodingKey {
//        case the67696D537E07Ad54368A45E6E9A79Fd6 = "67696d537e07ad54368a45e6e9a79fd6"
//    }
//}
//
//
//// MARK: Encode/decode helpers
//
//class JSONNull: Codable {
//    public init() {}
//
//    public required init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if !container.decodeNil() {
//            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
//        }
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        try container.encodeNil()
//    }
//}
//
//class JSONCodingKey: CodingKey {
//    let key: String
//
//    required init?(intValue: Int) {
//        return nil
//    }
//
//    required init?(stringValue: String) {
//        key = stringValue
//    }
//
//    var intValue: Int? {
//        return nil
//    }
//
//    var stringValue: String {
//        return key
//    }
//}
//
//class JSONAny: Codable {
//    public let value: Any
//
//    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
//        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
//        return DecodingError.typeMismatch(JSONAny.self, context)
//    }
//
//    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
//        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
//        return EncodingError.invalidValue(value, context)
//    }
//
//    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
//        if let value = try? container.decode(Bool.self) {
//            return value
//        }
//        if let value = try? container.decode(Int64.self) {
//            return value
//        }
//        if let value = try? container.decode(Double.self) {
//            return value
//        }
//        if let value = try? container.decode(String.self) {
//            return value
//        }
//        if container.decodeNil() {
//            return JSONNull()
//        }
//        throw decodingError(forCodingPath: container.codingPath)
//    }
//
//    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
//        if let value = try? container.decode(Bool.self) {
//            return value
//        }
//        if let value = try? container.decode(Int64.self) {
//            return value
//        }
//        if let value = try? container.decode(Double.self) {
//            return value
//        }
//        if let value = try? container.decode(String.self) {
//            return value
//        }
//        if let value = try? container.decodeNil() {
//            if value {
//                return JSONNull()
//            }
//        }
//        if var container = try? container.nestedUnkeyedContainer() {
//            return try decodeArray(from: &container)
//        }
//        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
//            return try decodeDictionary(from: &container)
//        }
//        throw decodingError(forCodingPath: container.codingPath)
//    }
//
//    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
//        if let value = try? container.decode(Bool.self, forKey: key) {
//            return value
//        }
//        if let value = try? container.decode(Int64.self, forKey: key) {
//            return value
//        }
//        if let value = try? container.decode(Double.self, forKey: key) {
//            return value
//        }
//        if let value = try? container.decode(String.self, forKey: key) {
//            return value
//        }
//        if let value = try? container.decodeNil(forKey: key) {
//            if value {
//                return JSONNull()
//            }
//        }
//        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
//            return try decodeArray(from: &container)
//        }
//        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
//            return try decodeDictionary(from: &container)
//        }
//        throw decodingError(forCodingPath: container.codingPath)
//    }
//
//    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
//        var arr: [Any] = []
//        while !container.isAtEnd {
//            let value = try decode(from: &container)
//            arr.append(value)
//        }
//        return arr
//    }
//
//    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
//        var dict = [String: Any]()
//        for key in container.allKeys {
//            let value = try decode(from: &container, forKey: key)
//            dict[key.stringValue] = value
//        }
//        return dict
//    }
//
//    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
//        for value in array {
//            if let value = value as? Bool {
//                try container.encode(value)
//            } else if let value = value as? Int64 {
//                try container.encode(value)
//            } else if let value = value as? Double {
//                try container.encode(value)
//            } else if let value = value as? String {
//                try container.encode(value)
//            } else if value is JSONNull {
//                try container.encodeNil()
//            } else if let value = value as? [Any] {
//                var container = container.nestedUnkeyedContainer()
//                try encode(to: &container, array: value)
//            } else if let value = value as? [String: Any] {
//                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
//                try encode(to: &container, dictionary: value)
//            } else {
//                throw encodingError(forValue: value, codingPath: container.codingPath)
//            }
//        }
//    }
//
//    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
//        for (key, value) in dictionary {
//            let key = JSONCodingKey(stringValue: key)!
//            if let value = value as? Bool {
//                try container.encode(value, forKey: key)
//            } else if let value = value as? Int64 {
//                try container.encode(value, forKey: key)
//            } else if let value = value as? Double {
//                try container.encode(value, forKey: key)
//            } else if let value = value as? String {
//                try container.encode(value, forKey: key)
//            } else if value is JSONNull {
//                try container.encodeNil(forKey: key)
//            } else if let value = value as? [Any] {
//                var container = container.nestedUnkeyedContainer(forKey: key)
//                try encode(to: &container, array: value)
//            } else if let value = value as? [String: Any] {
//                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
//                try encode(to: &container, dictionary: value)
//            } else {
//                throw encodingError(forValue: value, codingPath: container.codingPath)
//            }
//        }
//    }
//
//    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
//        if let value = value as? Bool {
//            try container.encode(value)
//        } else if let value = value as? Int64 {
//            try container.encode(value)
//        } else if let value = value as? Double {
//            try container.encode(value)
//        } else if let value = value as? String {
//            try container.encode(value)
//        } else if value is JSONNull {
//            try container.encodeNil()
//        } else {
//            throw encodingError(forValue: value, codingPath: container.codingPath)
//        }
//    }
//
//    public required init(from decoder: Decoder) throws {
//        if var arrayContainer = try? decoder.unkeyedContainer() {
//            self.value = try JSONAny.decodeArray(from: &arrayContainer)
//        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
//            self.value = try JSONAny.decodeDictionary(from: &container)
//        } else {
//            let container = try decoder.singleValueContainer()
//            self.value = try JSONAny.decode(from: container)
//        }
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        if let arr = self.value as? [Any] {
//            var container = encoder.unkeyedContainer()
//            try JSONAny.encode(to: &container, array: arr)
//        } else if let dict = self.value as? [String: Any] {
//            var container = encoder.container(keyedBy: JSONCodingKey.self)
//            try JSONAny.encode(to: &container, dictionary: dict)
//        } else {
//            var container = encoder.singleValueContainer()
//            try JSONAny.encode(to: &container, value: self.value)
//        }
//    }
//}
