/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Line_items : Codable {
	let id : Int?
	let name : String?
	let product_id : Int?
	let variation_id : Int?
	let quantity : Int?
	let tax_class : String?
	let subtotal : String?
	let subtotal_tax : String?
	let total : String?
	let total_tax : String?
	let taxes : [Taxes]?
	let meta_data : [String]?
	let sku : String?
	let price : Int?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case product_id = "product_id"
		case variation_id = "variation_id"
		case quantity = "quantity"
		case tax_class = "tax_class"
		case subtotal = "subtotal"
		case subtotal_tax = "subtotal_tax"
		case total = "total"
		case total_tax = "total_tax"
		case taxes = "taxes"
		case meta_data = "meta_data"
		case sku = "sku"
		case price = "price"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		product_id = try values.decodeIfPresent(Int.self, forKey: .product_id)
		variation_id = try values.decodeIfPresent(Int.self, forKey: .variation_id)
		quantity = try values.decodeIfPresent(Int.self, forKey: .quantity)
		tax_class = try values.decodeIfPresent(String.self, forKey: .tax_class)
		subtotal = try values.decodeIfPresent(String.self, forKey: .subtotal)
		subtotal_tax = try values.decodeIfPresent(String.self, forKey: .subtotal_tax)
		total = try values.decodeIfPresent(String.self, forKey: .total)
		total_tax = try values.decodeIfPresent(String.self, forKey: .total_tax)
		taxes = try values.decodeIfPresent([Taxes].self, forKey: .taxes)
		meta_data = try values.decodeIfPresent([String].self, forKey: .meta_data)
		sku = try values.decodeIfPresent(String.self, forKey: .sku)
		price = try values.decodeIfPresent(Int.self, forKey: .price)
	}

}