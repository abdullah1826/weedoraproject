//
//  User.swift
//  Zahra Stores
//
//  Created by Muhammad Abdullah on 10/01/2020.
//  Copyright © 2020 AE Solutions - www.avicennaenterprise.com. All rights reserved.
//

import Foundation

struct User:Codable {
    
    var name: String?
    var phone: String?
    var email: String?
    var location: String?
    var suite: String?
    var state: String?
    var postalCode: String?
    var deliveryTime: String?
    var majorIntersections: String?
    var deliveryDetails: String?
    
}



struct PastOrders: Codable {
    var detail: String?
    var totalPrice: Double?
    var majorIntersections: String?
    var order_items : [Order_items]?
}

struct Order_items: Codable {
    var name: String?
    var price: String?
    var quantity: String?
}

