/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Json4Swift_Base : Codable {
	let id : Int?
	let name : String?
    let status : String?
    let short_description : String?
    var regular_price : String?
    let sale_price : String?
    let stock_status : String?
    var stock_quantity : String?
    let weight : String?
    let dimensions : Dimensions?
    let images : [ImageElement]?
    var price: String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
        case status = "status"
        case short_description = "short_description"
        case regular_price = "regular_price"
        case sale_price = "sale_price"
        case stock_status = "stock_status"
        case stock_quantity = "stock_quantity"
        case images = "images"
        case weight = "weight"
        case dimensions = "dimensions"
        case price = "price"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try? values.decodeIfPresent(Int.self, forKey: .id)
		name = try? values.decodeIfPresent(String.self, forKey: .name)
        status = try? values.decodeIfPresent(String.self, forKey: .status)
        short_description = try? values.decodeIfPresent(String.self, forKey: .short_description)
        price = try? values.decodeIfPresent(String.self, forKey: .price)
        regular_price = try? values.decodeIfPresent(String.self, forKey: .regular_price)
        sale_price = try? values.decodeIfPresent(String.self, forKey: .sale_price)
        stock_status = try? values.decodeIfPresent(String.self, forKey: .stock_status)
        stock_quantity = try? values.decodeIfPresent(String.self, forKey: .stock_quantity)
        images = try? values.decodeIfPresent([ImageElement].self, forKey: .images)
        weight = try? values.decodeIfPresent(String.self, forKey: .weight)
        dimensions = try? values.decodeIfPresent(Dimensions.self, forKey: .dimensions)

	}

}


struct CartItems : Codable {
    var id : Int?
    var name : String?
    var status : String?
    var short_description : String?
    var regular_price : String?
    var sale_price : String?
    var stock_status : String?
    var weight : String?
    var dimensions : Dimensions?
    var images : [ImageElement]?
}
