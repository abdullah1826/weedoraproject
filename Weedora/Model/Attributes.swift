//
//  Attribute.swift
//  Zahra Stores
//
//  Created by Muhammad Abdullah on 04/12/2019.
//  Copyright © 2019 AE Solutions - www.avicennaenterprise.com. All rights reserved.
//

import Foundation
struct Attributes : Codable {
    let id : Int?
    let name : String?
    let option : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
        case option = "option"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        option = try values.decodeIfPresent(String.self, forKey: .option)
    }
    
}
