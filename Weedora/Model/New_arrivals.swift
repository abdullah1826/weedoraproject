/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct New_arrivals : Codable {
	let id : Int?
	let name : String?
	let status : String?
	let description : String?
	let short_description : String?
	let price : String?
	let regular_price : String?
	let sale_price : String?
	let price_html : String?
	let on_sale : Bool?
	let stock_quantity : Int?
	let stock_status : String?
    let related_ids : [Int]?
	let dimensions : Dimensions?
	let categories : [Categories]?
    let images : [ImageElement]?
	let variations : [Variations]?
	let regular_display_price : String?
	let sales_display_price : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case status = "status"
		case description = "description"
		case short_description = "short_description"
		case price = "price"
		case regular_price = "regular_price"
		case sale_price = "sale_price"
		case price_html = "price_html"
		case on_sale = "on_sale"
		case stock_quantity = "stock_quantity"
		case stock_status = "stock_status"
        case related_ids = "related_ids"
		case dimensions = "dimensions"
		case categories = "categories"
		case images = "images"
		case variations = "variations"
		case regular_display_price = "regular_display_price"
		case sales_display_price = "sales_display_price"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		short_description = try values.decodeIfPresent(String.self, forKey: .short_description)
		price = try values.decodeIfPresent(String.self, forKey: .price)
		regular_price = try values.decodeIfPresent(String.self, forKey: .regular_price)
		sale_price = try values.decodeIfPresent(String.self, forKey: .sale_price)
		price_html = try values.decodeIfPresent(String.self, forKey: .price_html)
		on_sale = try values.decodeIfPresent(Bool.self, forKey: .on_sale)
		stock_quantity = try values.decodeIfPresent(Int.self, forKey: .stock_quantity)
		stock_status = try values.decodeIfPresent(String.self, forKey: .stock_status)
		dimensions = try values.decodeIfPresent(Dimensions.self, forKey: .dimensions)
        related_ids = try values.decodeIfPresent([Int].self, forKey: .related_ids)
		categories = try values.decodeIfPresent([Categories].self, forKey: .categories)
		images = try values.decodeIfPresent([ImageElement].self, forKey: .images)
        variations = try values.decodeIfPresent([Variations].self, forKey: .variations)
		regular_display_price = try values.decodeIfPresent(String.self, forKey: .regular_display_price)
		sales_display_price = try values.decodeIfPresent(String.self, forKey: .sales_display_price)
	}

}
