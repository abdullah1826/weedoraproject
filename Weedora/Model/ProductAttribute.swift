//
//  ProductAttribute.swift
//  Zahra Stores
//
//  Created by Muhammad Abdullah on 04/12/2019.
//  Copyright © 2019 AE Solutions - www.avicennaenterprise.com. All rights reserved.
//

import Foundation
struct ProductAttribute : Codable {
    let id : Int?
    let name : String?
    let options : [String]?

    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
        case options = "options"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        options = try values.decodeIfPresent([String].self, forKey: .options)
    }
    
}
