//
//  VariationsImage.swift
//  Zahra Stores
//
//  Created by Muhammad Abdullah on 22/11/2019.
//  Copyright © 2019 AE Solutions - www.avicennaenterprise.com. All rights reserved.
//

import Foundation

struct VariationsImage: Codable {
    
    let id : Int?
    let date_created : String?
    let date_created_gmt : String?
    let date_modified : String?
    let date_modified_gmt : String?
    let src : String?
    let name : String?
    let alt : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case date_created = "date_created"
        case date_created_gmt = "date_created_gmt"
        case date_modified = "date_modified"
        case date_modified_gmt = "date_modified_gmt"
        case src = "src"
        case name = "name"
        case alt = "alt"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        date_created = try values.decodeIfPresent(String.self, forKey: .date_created)
        date_created_gmt = try values.decodeIfPresent(String.self, forKey: .date_created_gmt)
        date_modified = try values.decodeIfPresent(String.self, forKey: .date_modified)
        date_modified_gmt = try values.decodeIfPresent(String.self, forKey: .date_modified_gmt)
        src = try values.decodeIfPresent(String.self, forKey: .src)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        alt = try values.decodeIfPresent(String.self, forKey: .alt)
    }

}
