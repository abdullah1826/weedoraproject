//
//  Order.swift
//  Zahra Stores
//
//  Created by Muhammad Abdullah on 13/12/2019.
//  Copyright © 2019 AE Solutions - www.avicennaenterprise.com. All rights reserved.
//

import Foundation
struct Order: Codable {
    let id : Int?
    let parent_id : Int?
    let number : String?
    let order_key : String?
    let created_via : String?
    let version : String?
    let status : String?
    let currency : String?
    let date_created : String?
    let date_created_gmt : String?
    let date_modified : String?
    let date_modified_gmt : String?
    let discount_total : String?
    let discount_tax : String?
    let shipping_total : String?
    let shipping_tax : String?
    let cart_tax : String?
    let total : String?
    let total_tax : String?
    let prices_include_tax : Bool?
    let customer_id : Int?
    let customer_ip_address : String?
    let customer_user_agent : String?
    let customer_note : String?
    let billing : Billing?
    let shipping : Shipping?
    let payment_method : String?
    let payment_method_title : String?
    let transaction_id : String?
    let date_paid : String?
    let date_paid_gmt : String?
    let date_completed : String?
    let date_completed_gmt : String?
    let cart_hash : String?
    let meta_data : [Meta_data]?
    let line_items : [Line_items]?
    let tax_lines : [Tax_lines]?
    let shipping_lines : [Shipping_lines]?
    let fee_lines : [String]?
    let coupon_lines : [String]?
    let refunds : [String]?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case parent_id = "parent_id"
        case number = "number"
        case order_key = "order_key"
        case created_via = "created_via"
        case version = "version"
        case status = "status"
        case currency = "currency"
        case date_created = "date_created"
        case date_created_gmt = "date_created_gmt"
        case date_modified = "date_modified"
        case date_modified_gmt = "date_modified_gmt"
        case discount_total = "discount_total"
        case discount_tax = "discount_tax"
        case shipping_total = "shipping_total"
        case shipping_tax = "shipping_tax"
        case cart_tax = "cart_tax"
        case total = "total"
        case total_tax = "total_tax"
        case prices_include_tax = "prices_include_tax"
        case customer_id = "customer_id"
        case customer_ip_address = "customer_ip_address"
        case customer_user_agent = "customer_user_agent"
        case customer_note = "customer_note"
        case billing = "billing"
        case shipping = "shipping"
        case payment_method = "payment_method"
        case payment_method_title = "payment_method_title"
        case transaction_id = "transaction_id"
        case date_paid = "date_paid"
        case date_paid_gmt = "date_paid_gmt"
        case date_completed = "date_completed"
        case date_completed_gmt = "date_completed_gmt"
        case cart_hash = "cart_hash"
        case meta_data = "meta_data"
        case line_items = "line_items"
        case tax_lines = "tax_lines"
        case shipping_lines = "shipping_lines"
        case fee_lines = "fee_lines"
        case coupon_lines = "coupon_lines"
        case refunds = "refunds"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        parent_id = try values.decodeIfPresent(Int.self, forKey: .parent_id)
        number = try values.decodeIfPresent(String.self, forKey: .number)
        order_key = try values.decodeIfPresent(String.self, forKey: .order_key)
        created_via = try values.decodeIfPresent(String.self, forKey: .created_via)
        version = try values.decodeIfPresent(String.self, forKey: .version)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        date_created = try values.decodeIfPresent(String.self, forKey: .date_created)
        date_created_gmt = try values.decodeIfPresent(String.self, forKey: .date_created_gmt)
        date_modified = try values.decodeIfPresent(String.self, forKey: .date_modified)
        date_modified_gmt = try values.decodeIfPresent(String.self, forKey: .date_modified_gmt)
        discount_total = try values.decodeIfPresent(String.self, forKey: .discount_total)
        discount_tax = try values.decodeIfPresent(String.self, forKey: .discount_tax)
        shipping_total = try values.decodeIfPresent(String.self, forKey: .shipping_total)
        shipping_tax = try values.decodeIfPresent(String.self, forKey: .shipping_tax)
        cart_tax = try values.decodeIfPresent(String.self, forKey: .cart_tax)
        total = try values.decodeIfPresent(String.self, forKey: .total)
        total_tax = try values.decodeIfPresent(String.self, forKey: .total_tax)
        prices_include_tax = try values.decodeIfPresent(Bool.self, forKey: .prices_include_tax)
        customer_id = try values.decodeIfPresent(Int.self, forKey: .customer_id)
        customer_ip_address = try values.decodeIfPresent(String.self, forKey: .customer_ip_address)
        customer_user_agent = try values.decodeIfPresent(String.self, forKey: .customer_user_agent)
        customer_note = try values.decodeIfPresent(String.self, forKey: .customer_note)
        billing = try values.decodeIfPresent(Billing.self, forKey: .billing)
        shipping = try values.decodeIfPresent(Shipping.self, forKey: .shipping)
        payment_method = try values.decodeIfPresent(String.self, forKey: .payment_method)
        payment_method_title = try values.decodeIfPresent(String.self, forKey: .payment_method_title)
        transaction_id = try values.decodeIfPresent(String.self, forKey: .transaction_id)
        date_paid = try values.decodeIfPresent(String.self, forKey: .date_paid)
        date_paid_gmt = try values.decodeIfPresent(String.self, forKey: .date_paid_gmt)
        date_completed = try values.decodeIfPresent(String.self, forKey: .date_completed)
        date_completed_gmt = try values.decodeIfPresent(String.self, forKey: .date_completed_gmt)
        cart_hash = try values.decodeIfPresent(String.self, forKey: .cart_hash)
        meta_data = try values.decodeIfPresent([Meta_data].self, forKey: .meta_data)
        line_items = try values.decodeIfPresent([Line_items].self, forKey: .line_items)
        tax_lines = try values.decodeIfPresent([Tax_lines].self, forKey: .tax_lines)
        shipping_lines = try values.decodeIfPresent([Shipping_lines].self, forKey: .shipping_lines)
        fee_lines = try values.decodeIfPresent([String].self, forKey: .fee_lines)
        coupon_lines = try values.decodeIfPresent([String].self, forKey: .coupon_lines)
        refunds = try values.decodeIfPresent([String].self, forKey: .refunds)
    }
    
}
