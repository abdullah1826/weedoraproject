/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Shipping_lines : Codable {
	let id : Int?
	let method_title : String?
	let method_id : String?
	let total : String?
	let total_tax : String?
	let taxes : [String]?
	let meta_data : [String]?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case method_title = "method_title"
		case method_id = "method_id"
		case total = "total"
		case total_tax = "total_tax"
		case taxes = "taxes"
		case meta_data = "meta_data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		method_title = try values.decodeIfPresent(String.self, forKey: .method_title)
		method_id = try values.decodeIfPresent(String.self, forKey: .method_id)
		total = try values.decodeIfPresent(String.self, forKey: .total)
		total_tax = try values.decodeIfPresent(String.self, forKey: .total_tax)
		taxes = try values.decodeIfPresent([String].self, forKey: .taxes)
		meta_data = try values.decodeIfPresent([String].self, forKey: .meta_data)
	}

}