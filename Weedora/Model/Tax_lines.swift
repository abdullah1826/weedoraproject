/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Tax_lines : Codable {
	let id : Int?
	let rate_code : String?
	let rate_id : Int?
	let label : String?
	let compound : Bool?
	let tax_total : String?
	let shipping_tax_total : String?
	let meta_data : [String]?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case rate_code = "rate_code"
		case rate_id = "rate_id"
		case label = "label"
		case compound = "compound"
		case tax_total = "tax_total"
		case shipping_tax_total = "shipping_tax_total"
		case meta_data = "meta_data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		rate_code = try values.decodeIfPresent(String.self, forKey: .rate_code)
		rate_id = try values.decodeIfPresent(Int.self, forKey: .rate_id)
		label = try values.decodeIfPresent(String.self, forKey: .label)
		compound = try values.decodeIfPresent(Bool.self, forKey: .compound)
		tax_total = try values.decodeIfPresent(String.self, forKey: .tax_total)
		shipping_tax_total = try values.decodeIfPresent(String.self, forKey: .shipping_tax_total)
		meta_data = try values.decodeIfPresent([String].self, forKey: .meta_data)
	}

}