/*
 Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 
 For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar
 
 */

import Foundation

struct Variants : Codable {
    
    let id: Int?
    let dateCreated, dateCreatedGmt, dateModified, dateModifiedGmt: String?
    let variationDescription: String?
    let permalink: String?
    let sku, price, regularPrice, salePrice: String?
    let dateOnSaleFrom, dateOnSaleFromGmt, dateOnSaleTo, dateOnSaleToGmt: NSNull?
    let onSale: Bool?
    let status: String?
    let purchasable, virtual, downloadable: Bool?
    let downloads: [Any?]?
    let downloadLimit, downloadExpiry: Int?
    let taxStatus, taxClass: String?
    let manageStock: Bool?
    let stockQuantity: Int?
    let stockStatus, backorders: String?
    let backordersAllowed, backordered: Bool?
    let weight: String?
    let dimensions: Dimensions?
    let shippingClass: String?
    let shippingClassID: Int?
    let image: VariationImage?
    let attributes: [VariationAttribute]?
    let menuOrder: Int?
    //    let metaData: [VariationMetaDatum]?
    let variations: [Any?]?
    let taxAmount, regularDisplayPrice, salesDisplayPrice, barcode: String?
    
    
    
}
// MARK: - VariationAttribute
struct VariationAttribute: Codable {
    let id: Int?
    let name, option: String?
}

struct VariationImage: Codable {
    let id: Int?
    let dateCreated, dateCreatedGmt, dateModified, dateModifiedGmt: String?
    let src: String?
    let name, alt: String?
}

//    struct VariationMetaDatum: Decodable {
//        let id: Int?
//        let key: String?
//        let value: TentacledValue?
//    }
//

