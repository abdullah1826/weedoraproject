//
//  List.swift
//  Zahra Stores
//
//  Created by Muhammad Abdullah on 30/11/2019.
//  Copyright © 2019 AE Solutions - www.avicennaenterprise.com. All rights reserved.
//

import Foundation

struct List : Codable {
    let id : Int?
    let name : String?
    let status : String?
    let short_description : String?
    let regular_price : String?
    let sale_price : String?
    let on_sale : Bool?
    let stock_quantity : Int?
    let stock_status : String?
    let dimensions : Dimensions?
    let categories : [Categories]?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
        case status = "status"
        case short_description = "short_description"
        case regular_price = "regular_price"
        case sale_price = "sale_price"
        case on_sale = "on_sale"
        case stock_quantity = "stock_quantity"
        case stock_status = "stock_status"
        case dimensions = "dimensions"
        case categories = "categories"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        short_description = try values.decodeIfPresent(String.self, forKey: .short_description)
        regular_price = try values.decodeIfPresent(String.self, forKey: .regular_price)
        sale_price = try values.decodeIfPresent(String.self, forKey: .sale_price)
        on_sale = try values.decodeIfPresent(Bool.self, forKey: .on_sale)
        stock_quantity = try values.decodeIfPresent(Int.self, forKey: .stock_quantity)
        stock_status = try values.decodeIfPresent(String.self, forKey: .stock_status)
        dimensions = try values.decodeIfPresent(Dimensions.self, forKey: .dimensions)
        categories = try values.decodeIfPresent([Categories].self, forKey: .categories)
    }
    
}

