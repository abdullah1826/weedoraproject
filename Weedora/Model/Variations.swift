//
//  Variations.swift
//  Zahra Stores
//
//  Created by Muhammad Abdullah on 22/11/2019.
//  Copyright © 2019 AE Solutions - www.avicennaenterprise.com. All rights reserved.
//

import Foundation

struct Variations: Codable {
    
    let id : Int?
    let name : String?
    let status : String?
    let description : String?
    let short_description : String?
    let price : String?
    let regular_price : String?
    let sale_price : String?
    let price_html : String?
    let on_sale : Bool?
    let stock_quantity : Int?
    let stock_status : String?
    let sku: String?
    let image: VariationsImage?
    let attributes: [Attributes]?
    let regular_display_price : String?
    let sales_display_price : String?


    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
        case status = "status"
        case description = "description"
        case short_description = "short_description"
        case price = "price"
        case regular_price = "regular_price"
        case sale_price = "sale_price"
        case price_html = "price_html"
        case on_sale = "on_sale"
        case stock_quantity = "stock_quantity"
        case stock_status = "stock_status"
        case sku = "sku"
        case image = "image"
        case attributes = "attributes"
        case regular_display_price = "regular_display_price"
        case sales_display_price = "sales_display_price"

    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        short_description = try values.decodeIfPresent(String.self, forKey: .short_description)
        price = try values.decodeIfPresent(String.self, forKey: .price)
        regular_price = try values.decodeIfPresent(String.self, forKey: .regular_price)
        sale_price = try values.decodeIfPresent(String.self, forKey: .sale_price)
        price_html = try values.decodeIfPresent(String.self, forKey: .price_html)
        on_sale = try values.decodeIfPresent(Bool.self, forKey: .on_sale)
        stock_quantity = try values.decodeIfPresent(Int.self, forKey: .stock_quantity)
        sku = try values.decodeIfPresent(String.self, forKey: .sku)
        stock_status = try values.decodeIfPresent(String.self, forKey: .stock_status)
        image = try values.decodeIfPresent(VariationsImage.self, forKey: .image)
        attributes = try values.decodeIfPresent([Attributes].self, forKey: .attributes)
        regular_display_price = try values.decodeIfPresent(String.self, forKey: .regular_display_price)
        sales_display_price = try values.decodeIfPresent(String.self, forKey: .sales_display_price)
    }

}
