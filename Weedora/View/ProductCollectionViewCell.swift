//
//  ProductCollectionViewCell.swift
//  Weedora
//
//  Created by Muhammad Abdullah on 01/06/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {

       @IBOutlet weak var productImageVu: UIImageView!
       @IBOutlet weak var productName: UILabel!
       @IBOutlet weak var stockStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
