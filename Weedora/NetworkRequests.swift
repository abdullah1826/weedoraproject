//
//  NetworkRequests.swift
//  TacosTheBest
//
//  Created by Khawar Khan on 18/04/2020.
//  Copyright © 2020 AE-Solutions. All rights reserved.
//

import Foundation
import Alamofire
import UIKit
import ObjectMapper
import AlamofireObjectMapper

//class NetworkManager {
//    let placingorderurl = "http://tacos.twaintec.com/api/takeorder"
//    let userprofileupdateurl = "http://tacos.twaintec.com/api/updateuser/"
//    
//    // MARK: - Properties
//    private static var sharedNetworkManager: NetworkManager = {
//        let networkManager = NetworkManager()
//        return networkManager
//    }()
//    
//    // MARK: - Initialization
//    
//    private init(){
//        
//    }
//    
//    // MARK: - Accessors
//    class func shared() -> NetworkManager {
//        return sharedNetworkManager
//    }
//    
//    func Placingorder(params: [String : Any], onSuccess success: @escaping (_ user: PlaceOrder) -> Void, onFailure failure: @escaping (_ error: Error?) -> Void, onError message: @escaping (_ mess: String?) -> Void) {
//        
//        
//        Alamofire.request(placingorderurl,method: .post, parameters: params, encoding: URLEncoding.default,headers: nil).responseObject{(response:DataResponse<PlaceOrder>) in
//            
//            switch response.result {
//            case .success(let value):
//                success(value)
//            case .failure(let error):
//                failure(error)
//            }
//            
//        }
//    }
//    
//    func ProfileUpdate(userid: String, params: [String : Any], onSuccess success: @escaping (_ user: String?) -> Void, onFailure failure: @escaping (_ error: Error?) -> Void, onError message: @escaping (_ mess: String?) -> Void) {
//           
//           let finalurl : String = userprofileupdateurl + "user_id=\(userid)"
//           Alamofire.request(finalurl,method: .post, parameters: params, encoding: URLEncoding.default,headers: nil).responseObject{(response:DataResponse<updateprofile>) in
//               
//               switch response.result {
//               case .success(let value):
//                   if value.status == "true"{
//                    success(value.message)
//                   }else {
//                       message("Something went wrong, please try again...")
//                   }
//                   
//               case .failure(let error):
//                   failure(error)
//               }
//               
//           }
//       }
//    
//    func LOGIN(params: [String: Any], onSuccess success: @escaping (_ users: User?) -> Void, onFailure failure: @escaping (_ error: Error?) -> Void, onError error: @escaping (_ error: String?) -> Void) {
//        
//        let url = APIConstants.BasePath + APIPaths.login
//
//        Alamofire.request(url,method: .post, parameters: params, headers:nil).responseObject{(response:DataResponse<BASE_MAPPER>) in
//            
//            switch response.result {
//            case .success(let value):
//                if value.status == "true"{
//                    success(value.data)
//                }else {
//                    error(value.error)
//                }
//                
//            case .failure(let error):
//                failure(error)
//            }
//            
//        }
//    }
//    
//    func SIGNUP(params: [String: Any], onSuccess success: @escaping (_ users: User?) -> Void, onFailure failure: @escaping (_ error: Error?) -> Void, onError error: @escaping (_ error: String?) -> Void) {
//        
//        let url = APIConstants.BasePath + APIPaths.signup
//
//        Alamofire.request(url,method: .post, parameters: params, headers:nil).responseObject{(response:DataResponse<BASE_MAPPER>) in
//            
//            switch response.result {
//            case .success(let value):
//                if value.status == "true"{
//                    success(value.data)
//                }else {
//                    error(value.error)
//                }
//                
//            case .failure(let error):
//                failure(error)
//            }
//            
//        }
//    }
//    
//    
//    func GetMyOrders(onSuccess success: @escaping (_ users:[MyOrdersData]?) -> Void, onFailure failure: @escaping (_ error: Error?) -> Void, onError error: @escaping (_ error: String?) -> Void) {
//        
//        let url = APIConstants.BasePath + APIPaths.getorder + "/\(User.loggedUser!.user_id!)"
//
//        Alamofire.request(url,method: .get, parameters: nil, headers:nil).responseObject{(response:DataResponse<MyOrders>) in
//            
//            switch response.result {
//            case .success(let value):
//                if value.status == "true"{
//                    success(value.data)
//                }else {
//                    error(value.error)
//                }
//                
//            case .failure(let error):
//                failure(error)
//            }
//            
//        }
//    }
//
//    func resetPassword(params: [String: Any], onSuccess success: @escaping (_ users:String?) -> Void, onFailure failure: @escaping (_ error: Error?) -> Void, onError error: @escaping (_ error: String?) -> Void) {
//        
//        let url = APIConstants.BasePath + APIPaths.resetpassword
//
//        Alamofire.request(url,method: .post, parameters: params, headers:nil).responseObject{(response:DataResponse<MyOrders>) in
//            
//            switch response.result {
//            case .success(let value):
//                if value.status == "true"{
//                    if value.message == "Email sent!"{
//                        success(value.token)
//                    }else{
//                        success(value.message)
//                    }
//                }else {
//                    error(value.message)
//                }
//                
//            case .failure(let error):
//                failure(error)
//            }
//            
//        }
//    }
//
//}
